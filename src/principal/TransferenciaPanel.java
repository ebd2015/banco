package principal;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class TransferenciaPanel extends javax.swing.JInternalFrame {
	private JPanel panelTransfer;
	private JButton botonCancel;
	private JButton boton;
	private ATMPrincipal padre;
	private JTextField montoText;
	private JLabel montoLabel;
	private JTextField cajaText;
	private JLabel cajaLabel;
	private JButton check;
	private JLabel verificacionLabel;
	private Connection cnx;
	private int destino;
	private int cod_caja;
	private long nro_caja;
	private long nro_cliente;

	

	/**
	 * Constructor de la clase SeleccionarFechas
	 * @param padre de tipo ATMPrincipal, para comunicarse con dicha interface
	 */
	
	public TransferenciaPanel(ATMPrincipal padre, Connection cnx, int cod_caja) {
		
		super();
		this.padre = padre;
		this.cnx = cnx;
		this.cod_caja = cod_caja;
		initGUI();
	}
	
	public void setNro_caja(long nro_caja, long nro_cliente) {
		this.nro_caja = nro_caja;
		this.nro_cliente = nro_cliente;
	}
	/**
	 * inicializa la GUI para mostrar un panel de ingreso de datos 
	 */
	
	private void initGUI() {
		try {
			//Reestrinjo el cierre, y modificaciones sobre el size de la ventana
			this.setClosable(false);
			this.setMaximizable(false);
	        this.setResizable(false);

			{
				panelTransfer = new JPanel();
				getContentPane().add(panelTransfer, BorderLayout.CENTER);
				panelTransfer.setBackground(new java.awt.Color(255,255,128));
				panelTransfer.setLayout(null);
				panelTransfer.setPreferredSize(new java.awt.Dimension(369, 141));

				{
					boton = new JButton();
					panelTransfer.add(boton);
					boton.setText("Transferir");
					boton.setBounds(47, 107, 117, 23);
					
					//genera el String con las fechas y oculta la interfaz de ser correctos los datos ingresados
					boton.addActionListener(new OyenteBotonOK());
					boton.setEnabled(false);
				}
				{
					botonCancel = new JButton();
					panelTransfer.add(botonCancel);
					botonCancel.setText("CANCEL");
					botonCancel.setBounds(221, 107, 117, 23);
					//Oculta la interfaz
					botonCancel.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent arg0) {
							ocultar();
						}
					});
				}
				{
					check = new JButton();
					panelTransfer.add(check);
					check.setText("Verificar");
					check.setBounds(270, 12, 87, 23);
					check.addActionListener(new OyenteVerificar());
				}
				{
					cajaLabel = new JLabel();
					panelTransfer.add(cajaLabel);
					cajaLabel.setText("Caja Destino");
					cajaLabel.setBounds(12, 15, 79, 16);
				}
				{
					cajaText = new JTextField();
					panelTransfer.add(cajaText);
					cajaText.setBounds(96, 12, 162, 23);
					cajaText.addKeyListener(new OyenteTecla());
				}
				{
					verificacionLabel = new JLabel();
					panelTransfer.add(verificacionLabel);
					verificacionLabel.setText("Verificacion Correcta! Puede Proceder");
					verificacionLabel.setBounds(12, 43, 281, 17);
					verificacionLabel.setForeground(new java.awt.Color(0,20,200));
					verificacionLabel.setFont(new java.awt.Font("Segoe UI",0,12));

					verificacionLabel.setVisible(false);
				}
				{
					montoLabel = new JLabel();
					panelTransfer.add(montoLabel);
					montoLabel.setText("Monto:");
					montoLabel.setBounds(12, 71, 66, 16);
				}
				{
					montoText = new JTextField();
					panelTransfer.add(montoText);
					montoText.setBounds(96, 68, 162, 23);
					montoText.setEnabled(false);

				}
			}
			pack();
			this.setPreferredSize(new java.awt.Dimension(371, 166));
			
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	/**
	 * Oculta la interfaz de la vista una vez ingresados los datos correctos
	 */
	
	private void ocultar(){
		this.setBounds(0, 0, 371, 166);
		this.setVisible(false);
		verificacionLabel.setVisible(false);
		montoText.setEnabled(false);
		check.setEnabled(true);
		cajaText.setText("");
		montoText.setText("");
		boton.setEnabled(false);
	}
	
	private class OyenteVerificar implements ActionListener{

		

		public void actionPerformed(ActionEvent arg0) {
			try{

				String str = cajaText.getText();
				
				destino = new Integer(str);
				
				Statement stm = cnx.createStatement();
				
				stm.execute("select distinct nro_ca from trans_cajas_ahorro where nro_ca = "+destino);
			
				ResultSet res = stm.getResultSet();
				
				if(res.next() && destino != nro_caja){
					verificacionLabel.setVisible(true);
					montoText.setEnabled(true);
					check.setEnabled(false);
					boton.setEnabled(true);
				}
				else if(destino==nro_caja){
					JOptionPane.showMessageDialog(null,
			                "No se puede autotransferir dinero\n", 
			                "Error en los datos ingresados",
			                JOptionPane.ERROR_MESSAGE);
					}
					else{
						JOptionPane.showMessageDialog(null,
				                "El numero de caja ingresado no existe, no se puede realizar transferencia\n", 
				                "Error en los datos ingresados",
				                JOptionPane.ERROR_MESSAGE);					
					}
					
			}
			catch(NumberFormatException ex){
				JOptionPane.showMessageDialog(null,
		                "Por favor ingrese datos validos\n", 
		                "Error en los datos ingresados",
		                JOptionPane.ERROR_MESSAGE);
			} 
			catch (SQLException ex) {
				ex.printStackTrace();
			}
			
		}
		
	}
	
	
	private class OyenteBotonOK implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			try{
				String str = montoText.getText();
				
				if(str.lastIndexOf("-") == -1){
				
					float monto = new Float(str);
					
					if(monto != 0){
						
						Statement stm = cnx.createStatement();
						
						stm.execute("SET @mont = true");
						stm.execute("SET @verd = true");
						stm.execute("SET @bool_cli_caja = true");
						stm.execute("SET @bool_destino = true");
						
						
						
						str = "call transferencia("+nro_caja+", "+nro_cliente+", "+destino+", "+cod_caja+", "+monto+",@mont, @verd, @bool_cli_caja, @bool_destino)";
						
						stm.execute(str);
						
						stm.execute("select @verd, @bool_cli_caja, @bool_destino");
						
						ResultSet res = stm.getResultSet();
						
						res.next();
						
						if(res.getInt("@verd") == 1 && res.getInt("@bool_cli_caja") == 1 && res.getInt("@bool_destino") == 1){
						
							stm.execute("select @mont");
							
							res = stm.getResultSet();
							
							res.next();
							
							if(res.getInt("@mont") == 1){
							JOptionPane.showMessageDialog(null,
					                "La transferencia se ha realizado correctamente\n", 
					                "Transferencia Correcta",
					                JOptionPane.INFORMATION_MESSAGE);
							ocultar();
							
							padre.actualizar();
							destino = 0;
							}
							else{
								JOptionPane.showMessageDialog(null,
						                "Su saldo no es suficiente para realizar esta transaccion\n", 
						                "Error en los datos ingresados",
						                JOptionPane.ERROR_MESSAGE);
							}
						}
						
					
						else{
							JOptionPane.showMessageDialog(null,
					                "ERROR INTERNO", 
					                "Error en los datos ingresados",
					                JOptionPane.ERROR_MESSAGE);
						}
					}
					
					else{
						JOptionPane.showMessageDialog(null,
				                "El numero ingresado no se puede transferir", 
				                "Error en los datos ingresados",
				                JOptionPane.ERROR_MESSAGE);
					}
					
					
				}
				else{
					JOptionPane.showMessageDialog(null,
			                "Por favor ingrese datos validos\n", 
			                "Error en los datos ingresados",
			                JOptionPane.ERROR_MESSAGE);
				}
				
			}
			catch(NumberFormatException ex){
				JOptionPane.showMessageDialog(null,
		                "Por favor ingrese datos validos\n", 
		                "Error en los datos ingresados",
		                JOptionPane.ERROR_MESSAGE);
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null,
		                "ERROR INTERNO\n", 
		                "Error en los datos ingresados",
		                JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}
	
	private class OyenteTecla implements KeyListener{

		@Override
		public void keyPressed(KeyEvent arg0) {
			
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			check.setEnabled(true);
			verificacionLabel.setVisible(false);
			montoText.setEnabled(false);
			boton.setEnabled(false);
			
		}
		
	}
}
