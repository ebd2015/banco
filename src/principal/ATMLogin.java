package principal;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import javax.swing.WindowConstants;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class ATMLogin extends javax.swing.JInternalFrame {

	
	private long tarjeta;
	private JPanel principal;
	private Connection cnx;
	private JButton botonOk;
	private JButton botonCancel;
	private JTextField tarjetaField;
	private JPasswordField PINField;
	private JLabel labelTarjeta;
	private JLabel labelPIN;
	private JPanel panelIngreso;
	private ATMPrincipal vistaPrincipal;

	
	/**
	 * Constructor de la clase ATMLogin 
	 */
	
	public ATMLogin() {
		super();
		initGUI();
	}
	
	/**
	 * Inicializa la interface de login
	 */
	
	private void initGUI() {
		try {
			this.setClosable(true);
	        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	        this.setMaximizable(true);

	        this.setResizable(true);
	        
	        setTitle("Interfaz de ATM");
	        
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/banco?user=atm&&password=atm");
			
			{
				principal = new JPanel();
				getContentPane().add(principal, BorderLayout.CENTER);
				principal.setBackground(new java.awt.Color(255,255,128));
				principal.setLayout(null);
				
				{
					panelIngreso = new JPanel();
					principal.add(panelIngreso);
					panelIngreso.setBounds(0, 0, 284, 112);
					panelIngreso.setLayout(null);
					panelIngreso.setOpaque(false);
					{
						labelPIN = new JLabel();
						panelIngreso.add(labelPIN);
						labelPIN.setText("PIN");
						labelPIN.setBounds(8, 53, 59, 16);
					}
					{
						labelTarjeta = new JLabel();
						panelIngreso.add(labelTarjeta);
						labelTarjeta.setText("Nro Tarjeta");
						labelTarjeta.setBounds(8, 21, 71, 16);
					}
					{
						PINField = new JPasswordField();
						panelIngreso.add(PINField);
						PINField.setBounds(79, 51, 193, 20);
						//PINField.setText("charPIN01");
						PINField.addActionListener(new OyenteBotonOk());
					}
					{
						tarjetaField = new JTextField();
						panelIngreso.add(tarjetaField);
						tarjetaField.setBounds(79, 19, 193, 20);
						//tarjetaField.setText("45");
					}
					{
						botonCancel = new JButton();
						panelIngreso.add(botonCancel);
						botonCancel.setText("Cancel");
						botonCancel.setBounds(170, 81, 90, 20);
						botonCancel.addActionListener(new OyenteBotonCancel());
					}
					{
						botonOk = new JButton();
						panelIngreso.add(botonOk);
						botonOk.setText("OK");
						botonOk.setBounds(30, 81, 90, 20);
						botonOk.addActionListener(new OyenteBotonOk());
					}
					{
						vistaPrincipal = new ATMPrincipal(cnx);
						principal.add(vistaPrincipal);
						vistaPrincipal.setBounds(1,1,1,1);
						
						vistaPrincipal.setVisible(false);  
					}
				}
				
				
				
			}
			pack();
			setPreferredSize(new java.awt.Dimension(300,150));
			
			addComponentListener(new ComponentAdapter() {
	            public void componentHidden(ComponentEvent evt) {
	               cerrar();
	            }
	            public void componentShown(ComponentEvent evt) {
	               
	            }
	         });
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error en la inicializacion de ATMLogin",
                    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	/**
	 * Completa con 0s, hasta alcanzar la cantidad de largo, 
	 * el long tarjeta2 y lo devuelve en forma de String 
	 * @param tarjeta2, long con el numero a expandir
	 * @param largo, el largo del String de salida
	 * @return String resultado
	 */
	
	public static String CompletarCeros(long tarjeta2, int largo) {
		String aux = ""+tarjeta2;
		while(aux.length() < largo){
			aux = "0" + aux;
		}
		return aux;
	}
	
	/**
	 * Se utiliza para el boton Ok de la interfaz de login
	 * Corrobora que los datos ingresados sean correctos y de serlos muestra el resto de la interfaz
	 * @author
	 *
	 */
	
	private class OyenteBotonOk implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			try{
				
				String toTarjeta = tarjetaField.getText();
				if(toTarjeta.indexOf("-") == -1){
					tarjeta = new Integer(toTarjeta);
					
					String pin = new String(PINField.getPassword());
					
					String str = ""
							+"select nro_tarjeta"
							+"\nfrom Tarjeta t"
							+"\nwhere t.nro_tarjeta = " + tarjeta
							+"\nand t.PIN = md5(\"" + pin + "\")"
							;
					
					Statement stm = cnx.createStatement();
					stm.execute(str);
					ResultSet res = stm.getResultSet();
					if(res.next()){
						panelIngreso.setVisible(false);
						setSize(785,540);
						vistaPrincipal.setVisible(true);
						vistaPrincipal.setBounds(0, 0, vistaPrincipal.getPreferredSize().width, vistaPrincipal.getPreferredSize().height);
						vistaPrincipal.setTarjeta(tarjeta);
						setVisible(true);
					}
					else{
						panelIngreso.setVisible(false);
						tarjetaField.setVisible(false);
						labelTarjeta.setVisible(false);
						labelTarjeta = new JLabel();
						panelIngreso.add(labelTarjeta);
						labelTarjeta.setText("ERROR! ingrese nuevamente su PIN");
						labelTarjeta.setBounds(30, 19, 264, 16);
						labelTarjeta.setForeground(new java.awt.Color(255,0,0));
						labelTarjeta.setVisible(true);
						PINField.setText("");
						panelIngreso.setVisible(true);
					}
					
					stm.close();
					res.close();
				}
			}
			catch(SQLException ex){
				JOptionPane.showMessageDialog(null,
                        ex.getMessage() + "\n", 
                        "Error!",
                        JOptionPane.ERROR_MESSAGE);
			}
			catch(NumberFormatException ex){
				JOptionPane.showMessageDialog(null,
                        "Error en el numero ingresado.\nPor favor ingrese datos correctos", 
                        "Error en nro de Tarjeta",
                        JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
	
	/**
	 * Se utiliza para el boton Cancelar de la interface de login del ATMLogin
	 * @author
	 *
	 */
	
	private class OyenteBotonCancel implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			cerrar();
			
		}
		
	}
	
	
	
	/**
	 * Cierra la GUI de forma segura desconectandose de la base de datos
	 */
	
	public void cerrar() {
		desconectarBD();
		this.dispose();
		
	}
	
	/**
	 * Hace un close sobre la coneccion a la base de datos
	 */
	
	private void desconectarBD()
	   {
	         try
	         {
	            if(cnx!=null)
	            	cnx.close();  
	         }
	         catch (SQLException ex)
	         {
	        	 JOptionPane.showMessageDialog(null,
	                       ex.getMessage() + "\n", 
	                        "Error en desconectar la BD",
	                        JOptionPane.ERROR_MESSAGE);
	         }      
	   }

	
	
}