CREATE DATABASE banco;

USE banco; 

CREATE TABLE Ciudad (
	cod_postal INT(4) ZEROFILL UNSIGNED,
	# debe tener 4 digitos
	nombre VARCHAR(45) NOT NULL, 
	
	PRIMARY KEY (cod_postal)
	
) ENGINE=InnoDB;

CREATE TABLE Sucursal(
	nro_suc INT(3) ZEROFILL UNSIGNED AUTO_INCREMENT,
	# debe tener 3 digitos
	nombre VARCHAR(45) NOT NULL,
	direccion VARCHAR(45) NOT NULL,
	telefono VARCHAR(45) NOT NULL,
	horario VARCHAR(100)NOT NULL,
	
	cod_postal INT(4) UNSIGNED NOT NULL,
	
	PRIMARY KEY (nro_suc),
	
	CONSTRAINT FK_Ciudad_En_Sucursal
	FOREIGN KEY (cod_postal) REFERENCES Ciudad (cod_postal)
	ON DELETE RESTRICT ON UPDATE CASCADE
	#Revisar los delete y update
	
) ENGINE=InnoDB;

CREATE TABLE Empleado(
	legajo INT(4) ZEROFILL UNSIGNED AUTO_INCREMENT,
	apellido VARCHAR(45) NOT NULL,
	nombre VARCHAR(45) NOT NULL,
	tipo_doc VARCHAR(20) NOT NULL,
	nro_doc INT(8) UNSIGNED NOT NULL,
	direccion VARCHAR(45) NOT NULL,
	telefono VARCHAR(45) NOT NULL,
	cargo VARCHAR(45) NOT NULL,
	password VARCHAR(32) NOT NULL,
	#Se utiliza hash md5??
	
	nro_suc INT(3) UNSIGNED NOT NULL,
	
	PRIMARY KEY (legajo),
	
	CONSTRAINT FK_Sucursal_En_Empleado
	
	FOREIGN KEY (nro_suc) REFERENCES Sucursal (nro_suc)
	ON DELETE RESTRICT ON UPDATE CASCADE
	
) ENGINE=InnoDB;

CREATE TABLE Cliente(
	nro_cliente INT(5) ZEROFILL UNSIGNED AUTO_INCREMENT,
	# debe tener 5 digitos
	apellido VARCHAR(45) NOT NULL,
	nombre VARCHAR(45) NOT NULL,
	tipo_doc VARCHAR(20) NOT NULL,
	nro_doc INT(8) UNSIGNED NOT NULL,
	# debe tener 8 digitos
	direccion VARCHAR(45) NOT NULL,
	telefono VARCHAR(45) NOT NULL,
	fecha_nac DATE NOT NULL,
	
	PRIMARY KEY (nro_cliente)
	
) ENGINE=InnoDB;

CREATE TABLE Plazo_fijo(
	nro_plazo INT(8) ZEROFILL UNSIGNED AUTO_INCREMENT,
	# debe tener 8 digitos
	capital DECIMAL(16,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	fecha_inicio DATE NOT NULL,
	fecha_fin DATE NOT NULL,
	
	tasa_interes DECIMAL(4,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	interes DECIMAL(16,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	nro_suc INT(3) UNSIGNED NOT NULL,
	
	PRIMARY KEY (nro_plazo),
	
	CONSTRAINT FK_Sucursal_En_PlazoFijo
	FOREIGN KEY (nro_suc) REFERENCES Sucursal (nro_suc)
	ON DELETE RESTRICT ON UPDATE CASCADE
	
) ENGINE=InnoDB;

CREATE TABLE Tasa_Plazo_Fijo(
	periodo INT(3) UNSIGNED,
	monto_inf DECIMAL(16,2) UNSIGNED,
	#10 deberia ser un valor real
	monto_sup DECIMAL(16,2) UNSIGNED,
	#10 deberia ser un valor real
	tasa DECIMAL(4,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	
	PRIMARY KEY (periodo,monto_inf,monto_sup)
	
) ENGINE=InnoDB;

CREATE TABLE Plazo_Cliente(
	nro_plazo INT(8) UNSIGNED,
	nro_cliente INT(5) UNSIGNED,
	
	PRIMARY KEY (nro_plazo,nro_cliente),
	
	CONSTRAINT FK_Cliente_En_PlazoCliente
	FOREIGN KEY (nro_cliente) REFERENCES Cliente (nro_cliente)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_PlazoFijo_En_PlazoCliente
	FOREIGN KEY (nro_plazo) REFERENCES Plazo_Fijo (nro_plazo)
	ON DELETE RESTRICT ON UPDATE RESTRICT
	#Revisar los delete y update
	
) ENGINE=InnoDB;

CREATE TABLE prestamo(
	nro_prestamo INT(8) ZEROFILL UNSIGNED AUTO_INCREMENT,

	fecha DATE NOT NULL,
	cant_meses INT(2) UNSIGNED NOT NULL,
	
	monto DECIMAL(10,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	tasa_interes DECIMAL(4,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	interes DECIMAL(9,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	valor_cuota DECIMAL(9,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	
	legajo INT(4) UNSIGNED NOT NULL,
	
	nro_cliente INT(5) UNSIGNED NOT NULL,
	
	PRIMARY KEY (nro_prestamo),
	
	CONSTRAINT FK_Empleado_En_Prestamo
	FOREIGN KEY (legajo) REFERENCES Empleado (legajo)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_Cliente_En_Prestamo
	FOREIGN KEY (nro_cliente) REFERENCES Cliente (nro_cliente)
	ON DELETE RESTRICT ON UPDATE RESTRICT
	
) ENGINE=InnoDB;

CREATE TABLE Pago(
	nro_prestamo INT(8)  UNSIGNED,
	nro_pago INT(2)  UNSIGNED,
	fecha_venc DATE NOT NULL,
	fecha_pago DATE,
	
	PRIMARY KEY (nro_prestamo,nro_pago),
	
	CONSTRAINT FK_Prestamo_En_Pago
	FOREIGN KEY (nro_prestamo) REFERENCES Prestamo (nro_prestamo)
	ON DELETE RESTRICT ON UPDATE RESTRICT

) ENGINE=InnoDB;

CREATE TABLE Tasa_Prestamo(
	periodo INT(3) UNSIGNED,
	monto_inf DECIMAL(10,2) UNSIGNED,
	#10 deberia ser un valor real
	monto_sup DECIMAL(10,2) UNSIGNED,
	#10 deberia ser un valor real
	tasa DECIMAL(4,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real
	
	PRIMARY KEY (periodo,monto_inf,monto_sup)
	
) ENGINE=InnoDB;

CREATE TABLE Caja_Ahorro(
	nro_ca INT(8) ZEROFILL UNSIGNED AUTO_INCREMENT,
	CBU BIGINT(18) ZEROFILL UNSIGNED NOT NULL,
	saldo DECIMAL(16,2) UNSIGNED NOT NULL,
	#10 deberia ser un valor real revisar valor not null

	PRIMARY KEY (nro_ca)
	
) ENGINE=InnoDB;

CREATE TABLE Cliente_CA(
	nro_cliente INT(5)  UNSIGNED,
	nro_ca INT(8)  UNSIGNED,

	PRIMARY KEY (nro_cliente,nro_ca),
	
	CONSTRAINT FK_Cliente_En_Cliente_CA
	FOREIGN KEY (nro_cliente) REFERENCES Cliente (nro_cliente)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_CajaAhorro_En_Cliente_CA
	FOREIGN KEY (nro_ca) REFERENCES Caja_Ahorro (nro_ca)
	ON DELETE RESTRICT ON UPDATE RESTRICT
	#Revisar los delete y update
	
) ENGINE=InnoDB;

CREATE TABLE Tarjeta(
	nro_tarjeta BIGINT(16) ZEROFILL UNSIGNED AUTO_INCREMENT,
	PIN CHAR(32) NOT NULL,
	#se utiliza hash md5??
	CVT CHAR(32) NOT NULL,
	#se utiliza hash md5??
	fecha_venc DATE NOT NULL,
	nro_cliente INT(5) UNSIGNED NOT NULL,
	nro_ca INT(8)  UNSIGNED NOT NULL,

	PRIMARY KEY (nro_tarjeta),
	
	CONSTRAINT FK_Cliente_CA_En_Tarjeta
	FOREIGN KEY (nro_cliente,nro_ca) 
		REFERENCES Cliente_CA (nro_cliente,nro_ca)
	ON DELETE RESTRICT ON UPDATE RESTRICT
	
) ENGINE=InnoDB;

CREATE TABLE Caja(
	cod_caja INT(5) ZEROFILL UNSIGNED AUTO_INCREMENT,

	PRIMARY KEY (cod_caja)

) ENGINE=InnoDB;

CREATE TABLE Ventanilla(
	cod_caja INT(5)  UNSIGNED,
	nro_suc INT(3)  UNSIGNED NOT NULL,

	PRIMARY KEY (cod_caja),
	
	CONSTRAINT FK_Caja_En_Ventanilla
	FOREIGN KEY (cod_caja) REFERENCES Caja (cod_caja)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_Sucursal_En_Ventanilla
	FOREIGN KEY (nro_suc) REFERENCES Sucursal (nro_suc)
	ON DELETE RESTRICT ON UPDATE CASCADE

) ENGINE=InnoDB;

CREATE TABLE ATM(
	cod_caja INT(5)  UNSIGNED,
	cod_postal INT(4)  UNSIGNED NOT NULL,
	direccion VARCHAR(45) NOT NULL,
	
	PRIMARY KEY(cod_caja),
	
	CONSTRAINT FK_Caja_En_ATM
	FOREIGN KEY (cod_caja) REFERENCES Caja (cod_caja)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_Ciudad_En_ATM
	FOREIGN KEY (cod_postal) REFERENCES Ciudad (cod_postal)
	ON DELETE RESTRICT ON UPDATE RESTRICT

) ENGINE=InnoDB;


CREATE TABLE Transaccion(
	nro_trans INT(10) ZEROFILL UNSIGNED  AUTO_INCREMENT,
	fecha DATE NOT NULL,
	hora TIME(0) NOT NULL,
	monto DECIMAL(16,2) UNSIGNED NOT NULL,
	#10 deberia ser un real
	
	PRIMARY KEY(nro_trans)

) ENGINE=InnoDB;

CREATE TABLE Debito(
	nro_trans INT(10) UNSIGNED ,
	descripcion TEXT,
	nro_cliente INT(5) UNSIGNED NOT NULL,
	nro_ca INT(8) UNSIGNED NOT NULL,

	PRIMARY KEY (nro_trans),
	
	CONSTRAINT FK_Transaccion_En_Debito
	FOREIGN KEY (nro_trans) REFERENCES Transaccion (nro_trans)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_Cliente_CA_En_Debito
	FOREIGN KEY (nro_cliente,nro_ca) 
		REFERENCES Cliente_CA (nro_cliente,nro_ca)
	ON DELETE RESTRICT ON UPDATE RESTRICT

) ENGINE=InnoDB;

CREATE TABLE Transaccion_por_caja(
	nro_trans INT(10) UNSIGNED ,
	cod_caja INT(5) UNSIGNED NOT NULL,

	PRIMARY KEY(nro_trans),
	
	CONSTRAINT FK_transaccion_En_TransaccionPC
	FOREIGN KEY (nro_trans) REFERENCES Transaccion (nro_trans)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_Caja_En_TransaccionPC
	FOREIGN KEY (cod_caja) REFERENCES Caja (cod_caja)
	ON DELETE RESTRICT ON UPDATE RESTRICT

) ENGINE=InnoDB;

CREATE TABLE Deposito(
	nro_trans INT(10) UNSIGNED ,
	nro_ca INT(8) UNSIGNED NOT NULL,

	PRIMARY KEY(nro_trans),
	
	CONSTRAINT FK_Transaccion_por_caja_En_Deposito
	FOREIGN KEY (nro_trans) REFERENCES Transaccion_por_caja (nro_trans)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_Caja_Ahorro_En_Deposito
	FOREIGN KEY (nro_ca) REFERENCES Caja_Ahorro (nro_ca)
	ON DELETE RESTRICT ON UPDATE RESTRICT

) ENGINE=InnoDB;

CREATE TABLE Extraccion(
	nro_trans INT(10) UNSIGNED ,
	nro_cliente INT(5) UNSIGNED NOT NULL,
	nro_ca INT(8) UNSIGNED NOT NULL,

	PRIMARY KEY (nro_trans),
	
	CONSTRAINT FK_Transaccion_En_Extraccion
	FOREIGN KEY (nro_trans) REFERENCES Transaccion_por_caja (nro_trans)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_Cliente_CA_En_Extraccion
	FOREIGN KEY (nro_cliente,nro_ca) 
		REFERENCES Cliente_CA (nro_cliente,nro_ca)
	ON DELETE RESTRICT ON UPDATE RESTRICT

) ENGINE=InnoDB;

CREATE TABLE Transferencia(
	nro_trans INT(10) UNSIGNED ,
	nro_cliente INT(5) UNSIGNED NOT NULL,
	origen INT(8) UNSIGNED NOT NULL,
	destino INT(8) UNSIGNED NOT NULL,
	
	PRIMARY KEY (nro_trans),
	
	CONSTRAINT FK_Transaccion_En_Transferencia
	FOREIGN KEY (nro_trans) REFERENCES Transaccion_por_caja (nro_trans)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_Cliente_CA_En_Transferencia
	FOREIGN KEY (nro_cliente,origen) 
		REFERENCES Cliente_CA (nro_cliente,nro_ca)
	ON DELETE RESTRICT ON UPDATE RESTRICT,
	
	CONSTRAINT FK_CajaAhorro_En_Transferencia
	FOREIGN KEY (destino) REFERENCES Caja_Ahorro (nro_ca)
	ON DELETE RESTRICT ON UPDATE RESTRICT

) ENGINE=InnoDB;

######VIEW 

CREATE VIEW trans_cajas_ahorro AS
Select C.nro_ca, C.saldo,
	"NULL" AS nro_cliente, "NULL" AS tipo_doc, "NULL" AS nro_doc, "NULL" AS nombre, "NULL" AS apellido,
	Tr.nro_trans, Tr.fecha, Tr.hora, 
	"Deposito" AS tipo, 
	Tr.monto, TrCa.cod_caja, 
	"NULL" AS destino
	FROM Caja_Ahorro C, Deposito D, 
		Transaccion_por_caja TrCa, Transaccion Tr 
		Where C.nro_ca = D.nro_ca and
			Tr.nro_trans = TrCa.nro_trans and
			Tr.nro_trans = D.nro_trans	

UNION

Select C.nro_ca, C.saldo, 
	CL.nro_cliente, CL.tipo_doc, CL.nro_doc, CL.nombre, CL.apellido, 
	Tr.nro_trans, Tr.fecha, Tr.hora, 
	"Extraccion" AS tipo, 
	Tr.monto, TrCa.cod_caja, 
	"NULL" AS destino
	FROM Caja_Ahorro C, Cliente_CA CA, Cliente Cl,
			Transaccion_por_caja TrCa, Extraccion D, 
			Transaccion Tr
	Where C.nro_ca = CA.nro_ca and
			Cl.nro_cliente = CA.nro_cliente and
			D.nro_ca = CA.nro_ca and
			D.nro_cliente = CA.nro_cliente and
			Tr.nro_trans = TrCa.nro_trans and
			Tr.nro_trans = D.nro_trans	
		
UNION

Select C.nro_ca, C.saldo, 
	CL.nro_cliente, CL.tipo_doc, CL.nro_doc, CL.nombre, CL.apellido, 
	Tr.nro_trans, Tr.fecha, Tr.hora, 
	"Transferencia" AS tipo, 
	Tr.monto, TrCa.cod_caja, 
	D.destino 
	FROM 	Caja_Ahorro C, Cliente_CA CA, Cliente Cl,
			Transaccion_por_caja TrCa, Transferencia D, 
			Transaccion Tr
			Where C.nro_ca = CA.nro_ca and
			Cl.nro_cliente = CA.nro_cliente and
			D.origen = CA.nro_ca and
			D.nro_cliente = CA.nro_cliente and
			Tr.nro_trans = TrCa.nro_trans and
			Tr.nro_trans = D.nro_trans	

UNION

Select C.nro_ca, C.saldo, 
	CL.nro_cliente, CL.tipo_doc, CL.nro_doc, CL.nombre, CL.apellido, 
	Tr.nro_trans, Tr.fecha, Tr.hora, 
	"Debito" AS tipo, 
	Tr.monto, "NULL" AS cod_caja, 
	"NULL" AS destino
	FROM Caja_Ahorro C, Cliente_CA CA, Cliente Cl,
		Debito D, Transaccion Tr
		Where C.nro_ca = CA.nro_ca and
			Cl.nro_cliente = CA.nro_cliente and
			D.nro_ca = CA.nro_ca and
			D.nro_cliente = CA.nro_cliente and
			Tr.nro_trans = D.nro_trans	
			
UNION

Select C.nro_ca, C.saldo, 
	CL.nro_cliente, CL.tipo_doc, CL.nro_doc, CL.nombre, CL.apellido, 
	"NULL" as nro_trans, "NULL" AS fecha, "NULL" AS hora, 
	"NULL" AS tipo, 
	"NULL" AS monto, "NULL" AS cod_caja, 
	"NULL" AS destino
	FROM Caja_Ahorro C LEFT OUTER JOIN Cliente_CA CA ON CA.nro_ca = C.nro_ca LEFT OUTER JOIN Cliente Cl ON Cl.nro_cliente = CA.nro_cliente
		
		
;

######USER


DROP USER 'empleado'@'%';

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin';

GRANT ALL PRIVILEGES ON banco.* TO admin@localhost 
  IDENTIFIED BY 'admin' WITH GRANT OPTION;

CREATE USER'empleado'@'%' IDENTIFIED BY 'empleado';

GRANT SELECT ON banco.Empleado TO empleado@'%';
GRANT SELECT ON banco.Sucursal TO empleado@'%';
GRANT SELECT ON banco.Tasa_Plazo_Fijo TO empleado@'%';
GRANT SELECT ON banco.Tasa_Prestamo TO empleado@'%';
GRANT SELECT ON banco.Prestamo TO empleado@'%';
GRANT SELECT ON banco.Plazo_Fijo TO empleado@'%';
GRANT SELECT ON banco.Plazo_Cliente TO empleado@'%';
GRANT SELECT ON banco.Caja_Ahorro TO empleado@'%';
GRANT SELECT ON banco.Tarjeta TO empleado@'%';
GRANT SELECT ON banco.Cliente_CA TO empleado@'%';
GRANT SELECT ON banco.Cliente TO empleado@'%';
GRANT SELECT ON banco.Pago TO empleado@'%';

GRANT INSERT ON banco.Prestamo TO empleado@'%';
GRANT INSERT ON banco.Plazo_Fijo TO empleado@'%';
GRANT INSERT ON banco.Plazo_Cliente TO empleado@'%';
GRANT INSERT ON banco.Caja_Ahorro TO empleado@'%';
GRANT INSERT ON banco.Tarjeta TO empleado@'%';
GRANT INSERT ON banco.Cliente_CA TO empleado@'%';
GRANT INSERT ON banco.Cliente TO empleado@'%';
GRANT INSERT ON banco.Pago TO empleado@'%';

GRANT UPDATE ON Cliente_CA TO empleado@'%';
GRANT UPDATE ON Cliente TO empleado@'%';
GRANT UPDATE ON Pago TO empleado@'%'; 

CREATE USER atm@'%' IDENTIFIED BY 'atm';



GRANT SELECT ON trans_cajas_ahorro TO atm;
GRANT SELECT ON Tarjeta TO atm;
GRANT UPDATE ON Tarjeta TO atm; 

#Stored Procedures

delimiter !
CREATE PROCEDURE transferencia(IN origen INT(8),IN nro_cli INT(5), IN destino INT(8), IN cod_caja INT(5), IN monto DECIMAL(16,2) UNSIGNED, OUT mont BOOLEAN, OUT verd BOOLEAN, OUT bool_cli_caja BOOLEAN, OUT bool_destino BOOLEAN)
	BEGIN
		
		#declaro una variable para almacenar el saldo
		DECLARE s DECIMAL(16,2);
		#declaro el EXIT HANDLER
		DECLARE EXIT HANDLER FOR SQLEXCEPTION
			BEGIN
				ROLLBACK;
				SET verd = FALSE;
			END;
		#seteo variables booleanas para comprobacion
		
		SET bool_cli_caja = nro_cli in (select nro_cliente from cliente_ca where nro_ca = origen);
		SET bool_destino = destino in (select nro_ca from caja_ahorro);
		
		IF bool_cli_caja = 1 AND bool_destino = 1 THEN
			
			
			SELECT saldo INTO s FROM caja_ahorro WHERE nro_ca = origen;	
			
			SET mont = TRUE;
			SET verd = TRUE;
			
			IF s >= monto THEN
				
				START TRANSACTION;
				
					#modifico el saldo de la caja de ahorro origen
					SELECT saldo FROM caja_ahorro WHERE nro_ca = origen FOR UPDATE;
					UPDATE Caja_Ahorro SET saldo = saldo - monto WHERE nro_ca = origen;
					
					#modifico el saldo de la caja de ahorro destino
					SELECT saldo FROM caja_ahorro WHERE nro_ca = destino FOR UPDATE;
					UPDATE Caja_Ahorro SET saldo = saldo + monto WHERE nro_ca = destino;
				
					#inserto una nueva transaccion y transaccion_por_caja relacionadas
					INSERT INTO Transaccion VALUES(NULL, CURDATE(), CURTIME(), monto);
					INSERT INTO Transaccion_por_caja VALUES(LAST_INSERT_ID(), cod_caja);
				
					#inserto una nueva transferencia
					INSERT INTO Transferencia VALUES(LAST_INSERT_ID(), nro_cli, origen, destino);
				
					#inserto una nueva transaccion y transaccion_por_caja relacionadas
					INSERT INTO Transaccion VALUES(NULL, CURDATE(), CURTIME(), monto);
					INSERT INTO Transaccion_por_caja VALUES(LAST_INSERT_ID(), cod_caja);
				
					#inserto un nuevo deposito
					INSERT INTO Deposito VALUES(LAST_INSERT_ID(), destino);
				COMMIT;
			ELSE SET mont = FALSE;
			END IF;
		END IF;
	END; !
delimiter ;

GRANT EXECUTE ON PROCEDURE transferencia TO atm;

delimiter !
CREATE PROCEDURE extraccion (IN caja INT(8),IN nro_cli INT(5), IN cod_caja INT(5), IN monto DECIMAL(16,2) UNSIGNED, OUT mont BOOLEAN, OUT bool_cli_caja BOOLEAN)
	BEGIN
		
		#declaro 2 variables para almacenar caja y nro_cliente
		DECLARE s DECIMAL(16,2);
		
		#seteo una variable booleana para comprobar que el cliente sea due�o de la caja
		
		SET bool_cli_caja = nro_cli in (select nro_cliente from cliente_ca where nro_ca = caja);
		
		IF bool_cli_caja = 1 THEN
		
		
			SET mont = TRUE;
			
			#recupero el saldo
			
			SELECT saldo INTO s FROM caja_ahorro WHERE nro_ca = caja;
			
			IF s >= monto THEN
				
				START TRANSACTION;
					#modifico el saldo de la caja de ahorro
					SELECT saldo FROM caja_ahorro WHERE nro_ca = caja FOR UPDATE;
					UPDATE Caja_Ahorro SET saldo = saldo - monto WHERE nro_ca = caja;
				
					#inserto una nueva transaccion y transaccion_por_caja relacionadas
					INSERT INTO Transaccion VALUES(NULL, CURDATE(), CURTIME(), monto);
					INSERT INTO Transaccion_por_caja VALUES(LAST_INSERT_ID(), cod_caja);
				
					#inserto una nueva transferencia
					INSERT INTO Extraccion VALUES(LAST_INSERT_ID(), nro_cli, caja);
				COMMIT;
			ELSE SET mont = FALSE;
			END IF;
		END IF;
	END; !
delimiter ;

GRANT EXECUTE ON PROCEDURE extraccion TO atm;



delimiter !
CREATE TRIGGER nuevoPrestamo 
AFTER INSERT ON Prestamo
FOR EACH ROW
BEGIN
	DECLARE nropagos INT(2) UNSIGNED; 
	DECLARE idPrestamo int(8) UNSIGNED;
	DECLARE contador INT(3);
	SET contador = 1;
	
	SELECT cant_meses INTO nropagos FROM Prestamo WHERE nro_prestamo = NEW.nro_prestamo;
	
	simple_loop: LOOP
		INSERT INTO Pago VALUES(NEW.nro_prestamo,contador, DATE_ADD(CURDATE(), INTERVAL contador MONTH) , NULL);
		SET contador=contador+1;
		IF(contador>nropagos) THEN LEAVE simple_loop;
		END IF;
	END LOOP simple_loop;
	
END !
delimiter ;








