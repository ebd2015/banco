package principal;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDesktopPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/

@SuppressWarnings("serial")
public class Aplication extends javax.swing.JFrame {

	private AdminLogin adminPanel;
	private ATMLogin ATMPanel;
	private EmpleadoLogin empleadoPanel;
	private JMenuBar menuBar;
	private JMenuItem itemPrestamos;
	private JMenuItem itemATM;
	private JMenuItem ItemAdm;
	private JMenu jMenu1;
	private JDesktopPane deskPane;
	
	/**
	* Auto-generated main method to display this JFrame
	*/
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Aplication inst = new Aplication();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}
	
	/**
	 * Constructor de la clase Aplication
	 */
	
	public Aplication() {
		super();
		initGUI();
	}
	
	/**
	 * Inicializa la GUI vacia con el menu de opciones
	 */
	
	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			setTitle("Proyecto EBD 2015");
			{
				menuBar = new JMenuBar();
				setJMenuBar(menuBar);
				{
					jMenu1 = new JMenu();
					menuBar.add(jMenu1);
					jMenu1.setText("Funcionalidades");
					{
						ItemAdm = new JMenuItem();
						jMenu1.add(ItemAdm);
						ItemAdm.setText("Administrar BD (a)");
						ItemAdm.addActionListener(new ActionListener(){
							public void actionPerformed(ActionEvent arg0) {
							
								adminPanel = new AdminLogin();
								
								deskPane.add(adminPanel);
								adminPanel.setBounds(0,0,280, 100);
								
								adminPanel.setVisible(true);  
							}
							
						});
					}
					{
						itemATM = new JMenuItem();
						jMenu1.add(itemATM);
						itemATM.setText("ATM (b)");
						itemATM.addActionListener(new ActionListener(){

							public void actionPerformed(ActionEvent arg0) {
							
								ATMPanel = new ATMLogin();
								
								deskPane.add(ATMPanel);
								ATMPanel.setBounds(0,0,300, 150);
								
								ATMPanel.setVisible(true);  
							}
							
						});
					}
					{
						itemPrestamos = new JMenuItem();
						jMenu1.add(itemPrestamos);
						itemPrestamos.setText("Administrar Prestamos (c)");
						itemPrestamos.addActionListener(new ActionListener(){

							public void actionPerformed(ActionEvent arg0) {
							
								empleadoPanel = new EmpleadoLogin();
								
								deskPane.add(empleadoPanel);
								empleadoPanel.setBounds(0,0,300, 150);
								
								empleadoPanel.setVisible(true);  
							}
							
						});
					}
				}
			}
			{
				deskPane = new JDesktopPane();
				getContentPane().add(deskPane, BorderLayout.CENTER);
				deskPane.setBackground(new java.awt.Color(0,0,0));
			}
			pack();
			setSize(800, 600);
			
			this.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent evt) {
					if(adminPanel != null)
						adminPanel.cerrar();
					if(ATMPanel != null)
						ATMPanel.cerrar();
				}
			});
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}

}
