package principal;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class EmpleadoPrincipal extends JPanel {
	
	private EmpleadoLogin login;
	
	/**
	 * Create the panel.
	 * @param cnx 
	 */
	public EmpleadoPrincipal(EmpleadoLogin login) {
		this.login=login;
		setLayout(null);
		setBackground(new java.awt.Color(0,128,255));
		
		setPreferredSize(new Dimension(224, 180));
		
		setOpaque(false);
		
		JPanel panelBotones = new JPanel();
		panelBotones.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelBotones.setBounds(10, 45, 204, 120);
		add(panelBotones);
		panelBotones.setLayout(null);
		
		JButton PrestamoBtn = new JButton("Nuevo prestamo");
		PrestamoBtn.setBounds(10, 11, 184, 23);
		panelBotones.add(PrestamoBtn);
		PrestamoBtn.addActionListener(new OyenteNuevoPrestamo());
		
		JButton btnRegistrarPagoDe = new JButton("Registrar pago de cuota");
		btnRegistrarPagoDe.setBounds(10, 45, 184, 23);
		panelBotones.add(btnRegistrarPagoDe);
		btnRegistrarPagoDe.addActionListener(new OyenteRegistrarPago());
		
		JButton btnListarClientesMorosos = new JButton("Listar clientes morosos");
		btnListarClientesMorosos.setBounds(10, 79, 182, 23);
		panelBotones.add(btnListarClientesMorosos);
		btnListarClientesMorosos.addActionListener(new OyenteListarMorosos());
		
		JPanel panelTexto = new JPanel();
		panelTexto.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelTexto.setBounds(10, 11, 204, 19);
		add(panelTexto);
		panelTexto.setLayout(null);
		
		JLabel textoLabel = new JLabel("Qu\u00E9 operaci\u00F3n desea realizar? ");
		textoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		textoLabel.setBounds(0, 0, 204, 19);
		panelTexto.add(textoLabel);

	}
	
	private class OyenteNuevoPrestamo implements ActionListener{		

		public void actionPerformed(ActionEvent arg0) {		
			login.cargarPrestamo();

		}		
	}
	
	private class OyenteRegistrarPago implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {		
			login.registrarPago();
		}		
	}
	
	private class OyenteListarMorosos implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {		
			login.verMorosos();
		}		
	}
}
