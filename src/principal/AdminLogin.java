package principal;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.print.attribute.standard.JobOriginatingUserName;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import quick.dbtable.*;  

import java.sql.*;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class AdminLogin extends javax.swing.JInternalFrame {
	private JPanel principal;
	private JComboBox<String> tablasDesplegable;
	private JList<String> attrib;
	private JPasswordField password;
	private JTable tabla;
	
	private Connection cnx;
	private JButton updateButton;
	private JButton querryButton;
	private JLabel pass;
	
	private AdminConsultas querryPanel;
	private AdminUpdate updatePanel;
	
	private String ultimaConsulta;
	private JScrollPane ScrollPane;
	
	/**
	 * Constructor de la clase AdminLogin 
	 */
	
	public AdminLogin() {
		super();
		
		initGUI();
	}
	
	/**
	 * Inicializa la GUI con el panel de login del administrador
	 */
	
	private void initGUI() {
		try {
			this.setClosable(true);
	        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	        this.setMaximizable(true);

	        this.setResizable(true);
	        setTitle("Interfaz de Administrador");
	        
			{
				createPrincipal();
				
				{
					password = new JPasswordField();
					principal.add(password);
					password.setBounds(97, 12, 163, 23);
					password.addActionListener(new OyentePassword());
					password.setToolTipText("Presione Enter para ingresar");
				}
				{
					pass = new JLabel();
					principal.add(pass);
					pass.setText("Contrase�a");
					pass.setBounds(12, 15, 98, 21);
					pass.setForeground(new java.awt.Color(255,255,255));
				}
				{
					// crea la tabla  
		        	tabla = new JTable();
					ScrollPane = new JScrollPane(tabla,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
					tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		        	
		        	//tabla.setEditable(false);  
		            
				}
			}
			pack();
			setPreferredSize(new java.awt.Dimension(200, 100));
			
			addComponentListener(new ComponentAdapter() {
	            public void componentHidden(ComponentEvent evt) {
	               cerrar();
	            }
	            public void componentShown(ComponentEvent evt) {
	               
	            }
	         });
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error en la inicializacion de la GUI",
                    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * crea el panel de fondo
	 */
	
	private void createPrincipal(){
		principal = new JPanel();
		
		getContentPane().add(principal, BorderLayout.CENTER);
		principal.setLayout(null);
		principal.setBackground(new java.awt.Color(0,100,100));
	}
	
	/**
	 * Inicializa el panel de administrador con los dos botones 'Consultar' y 'Modificacar' luego de un login correcto
	 */
	
	private void principalAdmin(){
		
		principal.setVisible(false);
		
		createPrincipal();
/*
		{
			querryButton = new JButton();
			principal.add(querryButton);
			querryButton.setText("Consultas");
			querryButton.setBounds(12, 12, 116, 23);
			querryButton.addActionListener(new OyenteConsultas());
		}
		{
			updateButton = new JButton();
			principal.add(updateButton);
			updateButton.setText("Modificar");
			updateButton.setBounds(140, 12, 116, 23);
			updateButton.addActionListener(new OyenteUpdate());
		}
	*/
		//ocultarUpdate();
		addQuerryPanel();
		setSize(785, 540);
		
		principal.setVisible(true);
	}
	
	/**
	 * Crea el menu de tablas para la interface de consultas
	 */
	
	private void createMenuTablasforQuerry() {
		/*
		 * Debe distinguirse el menu en las consultas del menu en las modificaciones
		 * ya que presentan distinta funcionalidad
		 * El menu de tablas en consulta muestra los atributos de la tabla seleccionada y se pueden ver 
		 * los distintos atributos haciendo doble click sobre uno de ellos
		 */
		
		{
			
			attrib = new JList<String>();
			principal.add(attrib);
			attrib.setBounds(12, 80, 148, 425);
			attrib.setBorder(new LineBorder(new java.awt.Color(190,0,0), 2, false));
			
		}
		{
			
			tablasDesplegable = new JComboBox<String>();
			principal.add(tablasDesplegable);
			tablasDesplegable.setBounds(12, 48, 148, 23);
			showTables();
			tablasDesplegable.addActionListener(new OyenteTablas());
			attrib.addMouseListener(new MouseAttrib());
			attrib.setToolTipText("Doble-Click para proyectar atributos");
		}
		attrib.setVisible(true);
		tablasDesplegable.setVisible(true);
	}
	
	/**
	 * Crea el menu de tablas para la interface de modificacion
	 */
	
	private void createMenuTablasforUpdate() {
		/*
		 * Debe distinguirse el menu en las consultas del menu en las modificaciones
		 * ya que presentan distinta funcionalidad
		 * El menu de tablas en la interface de modificaciones muestra la lista de atributos de la tabla selecionada y tambien muestra
		 * en la tabla grafica (DBTable) todas las tuplas de la tabla seleccionada
		 */
		{
			
			attrib = new JList<String>();
			principal.add(attrib);
			attrib.setBounds(12, 80, 148, 425);
			attrib.setBorder(new LineBorder(new java.awt.Color(190,0,0), 2, false));
			
		}
		{
			
			tablasDesplegable = new JComboBox<String>();
			principal.add(tablasDesplegable);
			tablasDesplegable.setBounds(12, 48, 148, 23);
			showTables();
			tablasDesplegable.addActionListener(new OyenteTablasUpdate());
		}
		attrib.setVisible(true);
		tablasDesplegable.setVisible(true);
	}
	
	/**
	 * Muestra el panel de consultas
	 */

	private void addQuerryPanel(){
	
		createMenuTablasforQuerry();
		querryPanel = new AdminConsultas(cnx, ScrollPane, this);
		principal.add(querryPanel);
		querryPanel.setBounds(0, 0, querryPanel.getPreferredSize().width, querryPanel.getPreferredSize().height);
		//updateButton.setEnabled(true);
		//querryButton.setEnabled(false);
		
	}
	
	
	/**
	 * Realiza las conecciones necesarias para trabajar con la BD
	 * @param pass password de administrados
	 * @throws SQLException
	 */
	
	private void conectarBD(String pass) throws SQLException
	   {
	         
	            String driver ="com.mysql.jdbc.Driver";
	        	String servidor = "localhost:3306";
	            String baseDatos = "banco";
	            String usuario = "admin";
	            String url = "jdbc:mysql://" + servidor + "/" + baseDatos;
	   
	           
	            try {
		           // tabla.connectDatabase(driver, url, usuario, pass);

					Class.forName("com.mysql.jdbc.Driver").newInstance();
					
		            cnx = DriverManager.getConnection(url+"?user="+usuario+"&&password="+pass);

				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException e) {
					JOptionPane.showMessageDialog(null,
	                        e.getMessage() + "\n", 
	                        "Error en la coneccion con la BD",
	                        JOptionPane.ERROR_MESSAGE);
				}
	    
	   }
	
	/**
	 * Cierra las conecciones con la BD
	 */
	
	private void desconectarBD(){
         try{
            //tabla.close();
            if(cnx!=null)
            	cnx.close();
         }
         catch (SQLException ex)
         {
        	 JOptionPane.showMessageDialog(null,
                       ex.getMessage() + "\n", 
                        "Error en desconectar la BD",
                        JOptionPane.ERROR_MESSAGE);
         }      
	}
	
	/**
	 * Muestra las Tablas de la BD en un JComboBox
	 */
	
	private void showTables(){
		try {
			
			Statement stm = cnx.createStatement();
			stm.execute("show tables");
			ResultSet res = stm.getResultSet();
			
			while (res.next()){
				//se agregan las tablas al menu desplegable
				tablasDesplegable.addItem(res.getString("Tables_in_banco"));
			}
			
			res.close();
			stm.close();
		
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error en la creacion de listas",
                    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Cierra la GUI de forma segura desconectandose de la base de datos
	 */
	
	public void cerrar() {
		desconectarBD();
		this.dispose();
		
	}
	
	/**
	 * Corrobora que los Tipos de dato de las columnas puedan ser parseados por la DBTable
	 * de no ser asi reemplaza cada valor de esa columna por un valor nulo
	 * @param str String con la consulta correspondiente
	 * @return String con la consulta modificada
	 * @throws SQLException
	 */
	
	
	private String solucionarConflictoTipos(String str) throws SQLException{
		Statement stm = cnx.createStatement();
		stm.execute(str);
		ResultSet res = stm.getResultSet();
		
		
		boolean corte = false;
		int i = 1;
		
		while(!corte && res.next()){
			while(!corte && i <= res.getMetaData().getColumnCount()){
				corte = (res.getMetaData().getColumnType(i) == -1);
				i++;
			}
			if(!corte)
				i = 1;
		}
		
		
		//res.first();
		if(corte){
			String columnProblem = res.getMetaData().getColumnName(i - 1);			
			
			String nuevo = " ";
			String var = "";
			
			i = 1;
			
			while(i <= res.getMetaData().getColumnCount()){
				var = res.getMetaData().getColumnName(i);
				if(var.equalsIgnoreCase(columnProblem))
					nuevo += " NULL as "+columnProblem+" , ";
				else
					nuevo += res.getMetaData().getColumnName(i)+" , ";
				i++;
			}
			
			nuevo = nuevo.substring(0, nuevo.length() - 2);
			
			int index = str.lastIndexOf(" * ");
			
			int indexF = index +2;
			
			if(index == -1){
				index = str.lastIndexOf(columnProblem);
				indexF = index + columnProblem.length();
			}
			
			String init = str.substring(0, index);
			
			String fin = str.substring(indexF,str.length());
			
			str =  init + nuevo + fin ;
			
		}
		return str;
	}
	
	/**
	 * Ejecuta en la base de datos una consulta que devuelve resultados
	 * y lo muestra en la tabla de resultados
	 * @param str consulta de sql
	 */
	
	public void ejecutarConsulta(String str) throws SQLException{
	  // seteamos la consulta a partir de la cual se obtendr�n los datos para llenar la tabla
    	
			//str = solucionarConflictoTipos(str);
		
			
			
			Statement stm = cnx.createStatement(); 
			stm.execute(str);
			
			final ResultSet res = stm.getResultSet();
			
			String[] array = createArrayColumnNames(res);
			

			TableModel Model =  // se crea un modelo de tabla PagosModel 
			        new DefaultTableModel  // extendiendo el modelo DefalutTableModel
			        (
			        		new String[][] {},
			        		array			        		
			        )
			        {	// con una clase an�nima 
			        	// define la clase java asociada a cada columna de la tabla
			        	Class<String>[] types = createArrayClass(res.getMetaData().getColumnCount());
			        	// define si una columna es editable
			        	boolean[] canEdit = createBool(res.getMetaData().getColumnCount());
			                      
			        	// recupera la clase java de cada columna de la tabla
			        	public Class getColumnClass(int columnIndex) {
			        		return types[columnIndex];
			        	}
			        	private Class<String>[] createArrayClass(int columnCount) throws SQLException {
			        		Class<String>[] cls = new Class[columnCount];
			        		for(int i =0; i<cls.length; i++){
			        			cls[i] = java.lang.String.class;
			        			
			        		}
			        		
			        		return cls;
			        	}

			        	
			        	private boolean[] createBool(int columnCount) {
							boolean[] toRet = new boolean[columnCount];
							
							for(int i = 0; i<toRet.length; i++){
								toRet[i] = false;
							}
							
							return toRet;
						}
						// determina si una celda es editable
			        	public boolean isCellEditable(int rowIndex, int columnIndex){
			        		return canEdit[columnIndex];
			        	}
			        }
			;
			
			ScrollPane.setViewportView(tabla); 
			
	        tabla.setModel(Model); // setea el modelo de la tabla  
	        tabla.setAutoCreateRowSorter(true); // activa el ordenamiento por columnas, para
	          
	        llenarTabla(res);
	        ultimaConsulta = str;
			//tabla.setSelectSql(str);
	    	  
	    	// obtenemos el modelo de la tabla a partir de la consulta para 
	    	// modificar la forma en que se muestran de algunas columnas  
	    	//tabla.createColumnModelFromQuery();    
	    	//tabla.refresh();
	        res.close();
	        stm.close();
	
	}
	
	private void llenarTabla(ResultSet res) throws SQLException{	        //Se crea la tabla
		    // se recorre el resulset y se actualiza la tabla en pantalla
	        ((DefaultTableModel) this.tabla.getModel()).setRowCount(1);
	        int i = 0;
	        
	        
	       /* 
	        while(i<res.getMetaData().getColumnCount()){
	        	this.tabla.setValueAt(res.getMetaData().getColumnName(i+1), 0, i);
	        	i++;
	        }
	        
	        i = 0;
	        */
	        while (res.next()) {
	        	// agrega una fila al modelo de la tabla
	        	((DefaultTableModel) this.tabla.getModel()).setRowCount(i + 1);
	        	for(int j = 0; j<res.getMetaData().getColumnCount(); j++){
	        		this.tabla.setValueAt(res.getString(res.getMetaData().getColumnName(j+1)), i, j);
	        	}
	        	i++;
	        }
		
	}
	
	private String[] createArrayColumnNames(ResultSet res) throws SQLException {
		String[] array = new String[res.getMetaData().getColumnCount()];
		String aux = "";
		for(int i = 0; i< array.length; i++){
			aux = res.getMetaData().getColumnName(i+1);
			array[i] = aux;
		}
		
		return array;
	}

	
	/**
	 * Oyente para la el menu de tablas de la interface de consultas
	 * @author 
	 *
	 */
	
	
	private class OyenteTablas implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			try{
				Statement stm = cnx.createStatement();
				
				String tab = tablasDesplegable.getSelectedItem().toString();
				
				DefaultListModel<String> attribModel = new DefaultListModel<String>();
				
				stm.execute("describe "+tab);
				ResultSet res = stm.getResultSet();
				
				while (res.next()){
					attribModel.addElement(res.getString("Field"));
				}
				attrib.setModel(attribModel);
				res.close();
				stm.close();
		    }
			catch (SQLException ex){
				JOptionPane.showMessageDialog(null,
                        ex.getMessage() + "\n", 
                        "Error!",
                        JOptionPane.ERROR_MESSAGE);
		    }
		}
	}
	

	/**
	 * Oyente para la el menu de tablas de la interface de modificaciones
	 * @author 
	 *
	 */
	
	private class OyenteTablasUpdate implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			try{
				Statement stm = cnx.createStatement();
				
				String tab = tablasDesplegable.getSelectedItem().toString();
				
				DefaultListModel<String> attribModel = new DefaultListModel<String>();
				
				stm.execute("describe "+tab);
				ResultSet res = stm.getResultSet();
				
				while (res.next()){
					attribModel.addElement(res.getString("Field"));
				}
				attrib.setModel(attribModel);
				res.close();
				stm.close();
				
				ejecutarConsulta("select * from "+tab);
				updatePanel.habilitarBotones();
		    }
			catch (SQLException ex){
				JOptionPane.showMessageDialog(null,
                        ex.getMessage() + "\n", 
                        "Error!",
                        JOptionPane.ERROR_MESSAGE);
		    }
		}
	}
	
	
	/**
	 * Ejecuta en la base de datos una consulta que no devuelve resultados
	 * @param str consulta de sql
	 * @throws SQLException 
	 */
	
	public void ejecutarMod(String str) throws SQLException {
		
			//Statement stm = cnx.createStatement();
			//stm.executeUpdate(str);
			//stm.execute(str);
			JOptionPane.showMessageDialog(null,
                    "Se ha ejecutado correctamente" + "\n", 
                    "Exito!",
                    JOptionPane.INFORMATION_MESSAGE);
			//stm.close();
			String aux = querryPanel.getConsulta();
			ocultarQuerry();
			ocultarTablas();
			addQuerryPanel();
			querryPanel.setConsulta(aux);
	   
	}
	
	/**
	 * Muestra en la tabla de resultados 
	 * los valores de los atrributos seleccionados con doble click
	 * @author
	 *
	 */
	
	private class MouseAttrib implements MouseListener{

		public void mouseClicked(MouseEvent evt) {
			
			
			if(evt.getClickCount() == 2){
				try{
					String cons = "select "+attrib.getSelectedValue().toString()+" from "+tablasDesplegable.getSelectedItem().toString();
							
					ejecutarConsulta(cons);
				}
				catch(SQLException ex){
					ex.printStackTrace();
				}
			}	
		}
		public void mouseEntered(MouseEvent arg0) {}
		public void mouseExited(MouseEvent arg0) {}
		public void mousePressed(MouseEvent arg0) {}
		public void mouseReleased(MouseEvent arg0) {}
		
	}
	

	
	/**
	 * Se utiliza para corroborar el password de admin
	 * @author
	 *
	 */
	
	private class OyentePassword implements ActionListener{

		
		public void actionPerformed(ActionEvent arg0) {
			
			try{
			
			conectarBD(new String(password.getPassword()));
			
			principalAdmin();
			
           // secundario.add(tabla);       
			}
			catch (SQLException ex){
	        	 JOptionPane.showMessageDialog(null,
                        ex.getMessage() + "\n", 
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
	         } 
			
		}
		
	}
	
	/**
	 * Oculta el panel de consultas
	 */
	
	private void ocultarQuerry() {
		if(querryPanel != null)
			querryPanel.setVisible(false);
		ocultarTablas();
	}
	
	/**
	 * Oculta el menu de tablas y la lista de atributos actual
	 */
	
	private void ocultarTablas(){
		if(attrib != null && tablasDesplegable != null){
			attrib.setVisible(false);
			tablasDesplegable.setVisible(false);
		}
	}

	public void refresh() {
		try{
		ejecutarConsulta(ultimaConsulta);
		}
		catch(SQLException ex){
			
		}
		
	}
	
	public JTable getTable(){
		return tabla;
	}
}


