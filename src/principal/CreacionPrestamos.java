package principal;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.text.SimpleDateFormat;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class CreacionPrestamos extends JPanel {
	private JPanel principal;
	private JButton verificar;
	private JButton crear;
	private JButton valorCuotaButton;
	private JLabel valorCuota;
	private JLabel valorCuotaLabel;
	private JLabel mesesLabel;
	private JComboBox<String> tipo;
	private JLabel tipoLayout;
	private JLabel montoLabel;
	private JLabel documentoLabel;
	private JComboBox<Integer> meses;
	private JTextField monto;
	private JTextField documento;
	private String clienteTipoDoc;
	private Connection cnx;
	private JButton botonCancel;
	private JLabel verificacionLabel;
	private JButton montoButton;
	private JPanel secundario;
	
	private int legajo = 1000;
	private long nro_cliente;
	
	private long clienteDoc;
	private int cant_meses;
	private float cant;
	private float interes;
	private float tasa_interes;
	private float valorDeCuota;
	private EmpleadoLogin login;
	
	public CreacionPrestamos(Connection cnx, int legajo, EmpleadoLogin login) {				
		super();
		this.cnx=cnx;
		this.legajo=legajo;
		this.login = login;
		initGUI();
	}
	
	private void initGUI() {
		try {
			
			//Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			//cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/banco?user=admin&&password=admin");
			
			{
				setLayout(null);
				setOpaque(false);
				
				principal = new JPanel();
				principal.setBounds(0,0, 380, 320);
				add(principal, BorderLayout.CENTER);
				principal.setLayout(null);
				principal.setOpaque(false);
				{
					documento = new JTextField();
					principal.add(documento);
					documento.setText("");
					documento.setBounds(133, 56, 80, 23);
				}
				{
					documentoLabel = new JLabel();
					principal.add(documentoLabel);
					documentoLabel.setText("Nro Documento");
					documentoLabel.setBounds(20, 59, 101, 17);
				}
				{
					tipoLayout = new JLabel();
					principal.add(tipoLayout);
					tipoLayout.setText("Tipo Documento");
					tipoLayout.setBounds(20, 24, 101, 17);
				}
				{
					
					tipo = new JComboBox<String>();
					principal.add(tipo);
					tipo.setBounds(133, 21, 69, 23);
					menuTipoDoc();

				}
				{
					verificar = new JButton();
					principal.add(verificar);
					verificar.setText("Verificar");
					verificar.setBounds(234, 56, 105, 23);
					verificar.addActionListener(new OyenteVerificar());
				}
				{
					botonCancel = new JButton();
					principal.add(botonCancel);
					botonCancel.setText("Cancelar");
					botonCancel.setBounds(188, 251, 151, 23);
					botonCancel.addActionListener(new OyenteBotonCancel());
				}
				{
					secundario = new JPanel();
					principal.add(secundario);
					secundario.setBounds(0, 0, 364, 282);
					secundario.setLayout(null);
					secundario.setOpaque(false);
					{
						monto = new JTextField();
						secundario.add(monto);
						monto.setBounds(133, 134, 80, 23);
					}
					{
						meses = new JComboBox<Integer>();
						secundario.add(meses);
						meses.setBounds(133, 173, 69, 23);
						//menuMeses();
					}
					{
						montoLabel = new JLabel();
						secundario.add(montoLabel);
						montoLabel.setText("Monto");
						montoLabel.setBounds(20, 137, 101, 17);
					}
					{
						mesesLabel = new JLabel();
						secundario.add(mesesLabel);
						mesesLabel.setText("Meses");
						mesesLabel.setBounds(20, 177, 101, 17);
					}
					{
						valorCuotaLabel = new JLabel();
						secundario.add(valorCuotaLabel);
						valorCuotaLabel.setText("Valor de cuota:");
						valorCuotaLabel.setBounds(20, 213, 101, 17);
					}
					{
						valorCuotaButton = new JButton();
						secundario.add(valorCuotaButton);
						valorCuotaButton.setText("Aceptar");
						valorCuotaButton.setBounds(234, 173, 105, 23);
						valorCuotaButton.addActionListener(new OyenteBotonCalcularCuota());
						valorCuotaButton.setEnabled(false);
						
					}
					{
						valorCuota = new JLabel();
						secundario.add(valorCuota);
						valorCuota.setText("---");
						valorCuota.setBounds(133, 213, 101, 17);
					}
					{
						crear = new JButton();
						secundario.add(crear);
						crear.setText("Registrar Prestamo");
						crear.setBounds(20, 251, 151, 23);
						crear.addActionListener(new OyenteAgregarPrestamo());
						crear.setEnabled(false);
					}
					{
						verificacionLabel = new JLabel();
						secundario.add(verificacionLabel);
						verificacionLabel.setText("Verificacion Correcta! Puede Proceder");
						verificacionLabel.setBounds(78, 98, 281, 17);
						verificacionLabel.setForeground(new java.awt.Color(21,9,185));
						verificacionLabel.setFont(new java.awt.Font("Segoe UI",0,12));
					}
					{
						montoButton = new JButton();
						secundario.add(montoButton);
						montoButton.setText("Ingresar");
						montoButton.setBounds(234, 134, 105, 23);
						montoButton.addActionListener(new OyenteMonto());
					}
					secundario.setVisible(false);
				}
			}
			setSize(380, 320);
			setPreferredSize(new Dimension(380, 320));
			
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	/**
	 * Cierra la GUI de forma segura desconectandose de la base de datos
	 */
	
	public void cerrar() {
		login.mostrarPrincipal(this);
	}
	
	
	
	private void menuMeses(){
		try {
			
			Statement stm = cnx.createStatement();
			stm.execute("select distinct periodo from Tasa_Prestamo");
			ResultSet res = stm.getResultSet();
			
			while (res.next()){
				//se agregan las tablas al menu desplegable
				meses.addItem(res.getInt("periodo"));
			}
			
			res.close();
			stm.close();
		
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error en la creacion de meses",
                    JOptionPane.ERROR_MESSAGE);
		}
	
	}
	
	private void menuTipoDoc(){
		try {
			
			Statement stm = cnx.createStatement();
			stm.execute("select distinct tipo_doc from cliente");
			ResultSet res = stm.getResultSet();
			
			while (res.next()){
				//se agregan las tablas al menu desplegable
				tipo.addItem(res.getString("tipo_doc"));
			}
			
			res.close();
			stm.close();
		
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error en la creacion de tipos de Doc",
                    JOptionPane.ERROR_MESSAGE);
		}
	
	}
	
	private void verificarMonto(){
		try{
			Statement stm = cnx.createStatement();
			
			stm.execute("select MAX(monto_sup) as sup from tasa_prestamo ");
			ResultSet res = stm.getResultSet();
			res.next();
			
			String toMont = monto.getText();
			if(toMont.indexOf("-") == -1){
				
				float max = res.getFloat("sup");
				cant = Math.round((new Float(toMont)) * 100.0f) / 100.0f ;
				
				if(cant <= max){
					menuMeses();
					valorCuotaButton.setEnabled(true);
					monto.setEditable(false);
					montoButton.setEnabled(false);
				}
				else{
					JOptionPane.showMessageDialog(null,
		                    "El monto supera el maximo permitido\nPor favor ingrese un monto menor", 
		                    "Error.",
		                    JOptionPane.ERROR_MESSAGE);
					
				}
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
		catch(NumberFormatException ex){
			JOptionPane.showMessageDialog(null,
	                "Error en el numero ingresado.\nPor favor ingrese datos correctos", 
	                "Error en nro de Tarjeta",
	                JOptionPane.ERROR_MESSAGE);
		}
	}

	private boolean verificacion(){
		boolean verd = false;
		try {
			clienteDoc = new Integer(documento.getText());
			clienteTipoDoc = (String) tipo.getSelectedItem();
			//pagos de el cliente clienteDoc cuya fecha de pago sea nula
			String str = ""
					+"select * from cliente c natural join prestamo p natural join pago cu"
					+" where c.nro_doc = "+clienteDoc
					+" and c.tipo_doc = \""+clienteTipoDoc+"\" "
					+" and cu.fecha_pago is NULL"
					;
			
			
			Statement stm = cnx.createStatement();
		
			stm.execute(str);
			ResultSet res = stm.getResultSet();
			
			//si hay algun pago con fecha de pago nula retorna true
			verd = res.next();
			
			res.close();
			stm.close();
		
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null,
                    "No se puede conectar con la BD\n", 
                    "Error en la verificacion",
                    JOptionPane.ERROR_MESSAGE);
		}
		catch (NumberFormatException e){
			JOptionPane.showMessageDialog(null,
                    "Error en el numero ingresado.\nPor favor ingrese datos correctos", 
                    "Error en la Verificacion",
                    JOptionPane.ERROR_MESSAGE);
		}
		return verd;
	}

	private void registrarCliente(){
		try{
			String str;
			str = ""
					+"select nro_cliente "
					+" from cliente "
					+" where nro_doc = "+clienteDoc
					+" and tipo_doc = \""+clienteTipoDoc+"\" ";
			
	
			Statement stm = cnx.createStatement();
			stm.execute(str);
			ResultSet res2 = stm.getResultSet();
			
			if(res2.next()){
				nro_cliente = res2.getInt("nro_cliente");
			
				secundario.setVisible(true);
				verificar.setEnabled(false);
				documento.setEditable(false);
				tipo.setEnabled(false);
			}
			else{
				JOptionPane.showMessageDialog(null,
	                    "El Cliente ingresado no se encuentra en la Base de Datos", 
	                    "Error.",
	                    JOptionPane.ERROR_MESSAGE);
			}
			
			res2.close();
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
	}

	private void agregarPrestamo() {
		
		int verd = JOptionPane.showConfirmDialog(this.getParent(), "Desea agregar el prestamo?", "Agregar CLiente", JOptionPane.YES_NO_OPTION);
		
		if(verd == JOptionPane.YES_OPTION){
			try{
				Statement stm = cnx.createStatement();
				java.util.Date hoy = new java.util.Date();
				
				String fechaAct = (new SimpleDateFormat("yyyy/MM/dd")).format(hoy);
				
				
				String str = ""
						+"insert into prestamo "
						+" values(NULL, "
						+"\""+fechaAct+"\", "
						+cant_meses+", "
						+cant+", "
						+tasa_interes+", "
						+interes+", "
						+valorDeCuota+", "
						+legajo+", "
						+nro_cliente+")"
						;
				
				stm.execute(str);
				
				
	//			agregarCuotas(fechaAct);			OJO TRIGGER 
				
				
				JOptionPane.showMessageDialog(null,
		                "El prestamo se ha registrado correcctamente", 
		                "Informacion",
		                JOptionPane.INFORMATION_MESSAGE);
				
				stm.close();
				cerrar();
			}
			catch (SQLException ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null,
		                ex.getMessage() + "\n", 
		                "Error en la consulta del monto maximo",
		                JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
	
	private void agregarCuotas(String fechaAct) throws SQLException{
		
		String str = ""
				+" select nro_prestamo from prestamo p"
				+" where p.nro_cliente = "+nro_cliente
				+" and p.legajo = "+legajo
				+" and p.nro_prestamo not in (select c.nro_prestamo from pago c)"
				;
			
		Statement stm = cnx.createStatement();
		
		stm.execute(str);
		ResultSet res = stm.getResultSet();
		
		
		res.next();
		int nro_prestamo = res.getInt("nro_prestamo");
		res.close();
		int i = 1;
		while (i <= cant_meses){
			
			str = "insert into pago values ("
			+nro_prestamo+", "
			+i+", "
			+"date_add(\""+fechaAct+"\", interval 1 month)"+", "
			+"NULL)"
				;	
			
			
			stm.execute(str);
			
			str = "select fecha_venc from pago "
					+" where nro_prestamo = "+nro_prestamo 
					+" and nro_pago = " +i;
				;					
			
			stm.execute(str);
			res = stm.getResultSet();
			if(res.next()){
				fechaAct = res.getString("fecha_venc");
				i++;
			}
			else{
				i = cant_meses + 1;
			}
		}
}

	private void calcularCuota(){
		try{
			Statement stm = cnx.createStatement();
			
			cant_meses = (Integer) meses.getSelectedItem();
					
			String str = ""
					+"select tasa "
					+" from tasa_prestamo"
					+" where monto_inf < " +Math.round(cant*100.0f)/100.0f
					+" and monto_sup >= " +Math.round(cant*100.0f)/100.0f
					+" and periodo = " +cant_meses;
					
			stm.execute(str);
			ResultSet res = stm.getResultSet();
			res.next();
					
			tasa_interes = res.getFloat("tasa"); 
					
			interes = Math.round((cant * tasa_interes * cant_meses) / 12)/100.0f;				
					
			valorDeCuota = Math.round((cant + interes) / cant_meses * 100.0f)/100.0f;
					
			valorCuota.setText(""+valorDeCuota);
					
			res.close();
			
			
			stm.close();
			
			meses.setEnabled(false);
			valorCuotaButton.setEnabled(false);
			
			crear.setEnabled(true);
			
		}
		catch (SQLException ex) {
			JOptionPane.showMessageDialog(null,
	                ex.getMessage() + "\n", 
	                "Error en el calculo de la cuota",
	                JOptionPane.ERROR_MESSAGE);
		}
		catch(NumberFormatException ex){
			JOptionPane.showMessageDialog(null,
	                "Error en el numero ingresado.\nPor favor ingrese datos correctos", 
	                "Error en nro de Tarjeta",
	                JOptionPane.ERROR_MESSAGE);
		}
	}

	private class OyenteMonto implements ActionListener{


		public void actionPerformed(ActionEvent arg0) {
			verificarMonto();
		}
	}
	
	private class OyenteVerificar implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			
			
			if(verificacion()){
				 JOptionPane.showMessageDialog(null,
	                     "El Cliente ingresado ya posee un prestamo\n", 
	                     "Error.",
	                     JOptionPane.ERROR_MESSAGE);
			}
			else{
				registrarCliente();
			}
			
		}
		
	}
	
	private class OyenteBotonCancel implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			cerrar();
			
		}
		
	}
	
	private class OyenteBotonCalcularCuota implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			calcularCuota();
		}
		
	}
	
	private class OyenteAgregarPrestamo implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			if(!verificacion()){
				agregarPrestamo();
			}
			else{
				JOptionPane.showMessageDialog(null,
		                "Este cliente posee otro prestamo registrado recientemente", 
		                "Error!",
		                JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
	
	
	

}
