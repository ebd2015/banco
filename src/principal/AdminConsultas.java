package principal;

import java.awt.Dimension;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.*;

import quick.dbtable.*;  


import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTable;




/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class AdminConsultas extends javax.swing.JPanel {

	private JPanel secundario;
	private JButton deleteText;
	private JButton execute;
	private JTextArea consultas;
	private JScrollPane consultasScrollPane;
	private JLabel adminLabel;
	private JScrollPane tabla;
	private AdminLogin padre;
	private Connection cnx;
	
	/**
	 * 
	 * @param cnx Connection de java.sql, una coneccion a la base de datos
	 * @param tabla DBTable que muestra los resultados de las consutas
	 * @param padre AdminLogin, JInternalFrame que contiene a este panel
	 */
	
	public AdminConsultas(Connection cnx, JScrollPane tabla, AdminLogin padre) {
		super();
		this.cnx = cnx;
		this.tabla = tabla;
		this.padre = padre;
		
		tabla.setBounds(12, 270, 583, 235);  
        
		initGUI();
	}
	
	/**
	 * Inicializa la GUI con el panel de Consultas de administrador
	 */
	
	private void initGUI() {
		try {
			setPreferredSize(new Dimension(785, 540));
			
			{
				secundario = new JPanel();
				this.add(secundario);
				secundario.setLayout(null);
				secundario.setBounds(166, 0, 617, 517);
				secundario.setBackground(new java.awt.Color(0,100,100));
				{
					deleteText = new JButton();
					secundario.add(deleteText);
					deleteText.setText("Borrar texto");
					deleteText.setBounds(346, 239, 116, 23);
					deleteText.addActionListener(new OyenteDelete());
				}
				{
					execute = new JButton();
					secundario.add(execute);
					execute.setText("Consultar");
					execute.setBounds(473, 239, 116, 23);
					execute.addActionListener(new OyenteConsulta());
				}
				{
					consultas = new JTextArea();
					consultas.setToolTipText("Ingrese su consulta en este cuadro");
					consultasScrollPane = new JScrollPane(consultas);
					secundario.add(consultasScrollPane);
					consultasScrollPane.setBounds(6, 50, 583, 177);
				}
				{
					adminLabel = new JLabel("Panel de Consultas para Base de Datos Banco");
					secundario.add(adminLabel);
							adminLabel.setBounds(152, 12, 369, 26);
							adminLabel.setFont(new java.awt.Font("Calibri",1,18));
							adminLabel.setForeground(new java.awt.Color(255,255,255));
				}
			}
			
			

			consultas.addKeyListener(new OyenteTextArea());
			
			secundario.add(tabla); 
			
			this.setLayout(null);
			this.setOpaque(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Se utiliza para el boton consultar
	 * distingue entre las consultas que arrojan resultados y las que no
	 * @author
	 *
	 */
	
	private class OyenteConsulta implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			try{
				String str = consultas.getText();
				
				Statement stm = cnx.createStatement();
				stm.execute(str);
				ResultSet res = stm.getResultSet();
				
				if(res != null)
					padre.ejecutarConsulta(str);
				else{
					padre.ejecutarMod(str);
					if(padre.getTable().getRowCount()>0)
						padre.refresh();
					
				}
				    
			}
			catch(SQLException ex){
				JOptionPane.showMessageDialog(null, ex.getMessage()+"\n", "Error en la consulta", JOptionPane.ERROR_MESSAGE);
			} 
			
		}
			
	}
	
	public String getConsulta(){
		return consultas.getText();
	}
	
	/**
	 * Se utiliza para el boton Borrar Texto
	 * Borra el contenido del Area de texto
	 * @author
	 *
	 */
	
	private class OyenteDelete implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			consultas.setText("");
		}
	}
	

	/**
	 * Ejecuta la consulta si hay un Enter y en el texto se percibe un ";"
	 * @author
	 *
	 */
	
	private class OyenteTextArea implements KeyListener{

		public void keyPressed(KeyEvent arg0) {
			int key = arg0.getKeyCode();
			if (key == 10){
				String str = consultas.getText();
				
				if(str.length() > 0 && str.indexOf(";") != -1){
					execute.doClick();
				}
			}
		}
		public void keyReleased(KeyEvent arg0) {}
		public void keyTyped(KeyEvent arg0) {}
	}

	public void setConsulta(String aux) {
		consultas.setText(aux);
	}
}
