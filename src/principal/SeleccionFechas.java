package principal;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class SeleccionFechas extends javax.swing.JInternalFrame {
	private JPanel panelFechas;
	private JLabel fechaMen;
	private JLabel FechaMay;
	private JTextField year2;
	private JTextField month2;
	private JTextField day2;
	private JButton botonCancel;
	private JButton boton;
	private JTextField day1;
	private JTextField month1;
	private JTextField year1;
	private ATMPrincipal padre;

	
	/**
	 * Constructor de la clase SeleccionarFechas
	 * @param padre de tipo ATMPrincipal, para comunicarse con dicha interface
	 */
	
	public SeleccionFechas(ATMPrincipal padre) {
		
		super();
		this.padre = padre;
		initGUI();
	}
	
	/**
	 * inicializa la GUI para mostrar un panel de ingreso de datos con dos fechas
	 * distinguidas en a�o, mes y dia 
	 */
	
	private void initGUI() {
		try {
			//Reestrinjo el cierre, y modificaciones sobre el size de la ventana
			this.setClosable(false);
			this.setMaximizable(false);
	        this.setResizable(false);

			{
				panelFechas = new JPanel();
				getContentPane().add(panelFechas, BorderLayout.CENTER);
				panelFechas.setBackground(new java.awt.Color(255,255,128));
				panelFechas.setLayout(null);
				{
					fechaMen = new JLabel();
					panelFechas.add(fechaMen);
					fechaMen.setText("Fecha inicial:");
					fechaMen.setBounds(12, 12, 74, 22);
				}
				{
					year1 = new JTextField();
					panelFechas.add(year1);
					year1.setText("AAAA");
					year1.setBounds(180, 12, 41, 22);
					
					//Borra el texto predeterminado
					year1.addMouseListener(new MouseListener(){
						public void mouseClicked(MouseEvent arg0) {}
						public void mouseEntered(MouseEvent arg0) {}
						public void mouseExited(MouseEvent arg0) {}
						public void mousePressed(MouseEvent arg0) {
							year1.setText("");
						}
						public void mouseReleased(MouseEvent arg0) {}
				    });
				}
				
				{
					month1 = new JTextField();
					panelFechas.add(month1);
					month1.setColumns(4);
					month1.setText("MM");
					month1.setBounds(139, 12, 29, 22);
					
					//Borra el texto predeterminado
					month1.addMouseListener(new MouseListener(){
						public void mouseClicked(MouseEvent arg0) {}
						public void mouseEntered(MouseEvent arg0) {}
						public void mouseExited(MouseEvent arg0) {}
						public void mousePressed(MouseEvent arg0) {
							month1.setText("");
						}
						public void mouseReleased(MouseEvent arg0) {}
				    });
				
				}
				{
					day1 = new JTextField();
					panelFechas.add(day1);
					day1.setColumns(4);
					day1.setText("DD");
					day1.setBounds(98, 12, 29, 22);
					
					//Borra el texto predeterminado
					day1.addMouseListener(new MouseListener(){
						public void mouseClicked(MouseEvent arg0) {}
						public void mouseEntered(MouseEvent arg0) {}
						public void mouseExited(MouseEvent arg0) {}
						public void mousePressed(MouseEvent arg0) {
							day1.setText("");
						}
						public void mouseReleased(MouseEvent arg0) {}
				    });
				}
				{
					year2 = new JTextField();
					panelFechas.add(year2);
					year2.setColumns(4);
					year2.setText("AAAA");
					year2.setBounds(180, 40, 41, 22);
					
					//Borra el texto predeterminado
					year2.addMouseListener(new MouseListener(){
						public void mouseClicked(MouseEvent arg0) {}
						public void mouseEntered(MouseEvent arg0) {}
						public void mouseExited(MouseEvent arg0) {}
						public void mousePressed(MouseEvent arg0) {
							year2.setText("");
						}
						public void mouseReleased(MouseEvent arg0) {}
				    });
				}
				{
					month2 = new JTextField();
					panelFechas.add(month2);
					month2.setColumns(4);
					month2.setText("MM");
					month2.setBounds(139, 40, 29, 22);
					
					//Borra el texto predeterminado
					month2.addMouseListener(new MouseListener(){
						public void mouseClicked(MouseEvent arg0) {}
						public void mouseEntered(MouseEvent arg0) {}
						public void mouseExited(MouseEvent arg0) {}
						public void mousePressed(MouseEvent arg0) {
							month2.setText("");
						}
						public void mouseReleased(MouseEvent arg0) {}
				    });
				}
				{
					day2 = new JTextField();
					panelFechas.add(day2);
					day2.setColumns(4);
					day2.setText("DD");
					day2.setBounds(98, 40, 29, 22);
					
					//Borra el texto predeterminado
					day2.addMouseListener(new MouseListener(){
						public void mouseClicked(MouseEvent arg0) {}
						public void mouseEntered(MouseEvent arg0) {}
						public void mouseExited(MouseEvent arg0) {}
						public void mousePressed(MouseEvent arg0) {
							day2.setText("");
						}
						public void mouseReleased(MouseEvent arg0) {}
				    });
				}
				{
					FechaMay = new JLabel();
					panelFechas.add(FechaMay);
					FechaMay.setText("Fecha final:");
					FechaMay.setBounds(12, 40, 74, 22);
				}
				{
					boton = new JButton();
					panelFechas.add(boton);
					boton.setText("OK");
					boton.setBounds(12, 79, 86, 23);
					
					//genera el String con las fechas y oculta la interfaz de ser correctos los datos ingresados
					boton.addActionListener(new ActionListener(){

						public void actionPerformed(ActionEvent arg0) {
							
							
							if(generarStringFechas())
								ocultar();
						}
					});
				}
				{
					botonCancel = new JButton();
					panelFechas.add(botonCancel);
					botonCancel.setText("CANCEL");
					botonCancel.setBounds(132, 79, 89, 23);
					//Oculta la interfaz
					botonCancel.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent arg0) {
							ocultar();
						}
					});
				}
			}
			pack();
			setPreferredSize(new java.awt.Dimension(250, 150));
			
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	/**
	 * Oculta la interfaz de la vista una vez ingresados los datos correctos
	 */
	
	private void ocultar(){
		this.setBounds(1, 1, 1, 1);
		this.setVisible(false);
		year2.setText("AAAA");
		month2.setText("MM");
		day2.setText("DD");
		year1.setText("AAAA");
		month1.setText("MM");
		day1.setText("DD");
		
	}
	
	/**
	 * genera un String con las fechas ingresadas y lo ingresa a la GUI principal ATMLogin
	 * @return true si la fecha es correcta, false en caso contrario
	 */
	
	private boolean generarStringFechas(){
		String date1 = day1.getText()+"/"+month1.getText()+"/"+year1.getText();
		String date2 = day2.getText()+"/"+month2.getText()+"/"+year2.getText();
		
		boolean ret = Fechas.validar(date1) && Fechas.validar(date2);
		
		if(ret){
			padre.ultimosMovimientos(date1, date2,0);
			
		}
			
		else{
			JOptionPane.showMessageDialog(null,
                    "Error al ingresar los datos" + "\n", 
                    "Error!",
                    JOptionPane.ERROR_MESSAGE);
		}
		return ret;
		
	}

}
