package principal;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import javax.swing.WindowConstants;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class EmpleadoLogin extends javax.swing.JInternalFrame {

	
	private int legajo;
	
	private JPanel principal;
	
	private Connection cnx;
	
	private JButton botonOk;
	private JButton botonCancel;
	
	private JTextField legajoField;
	private JPasswordField passwordField;
	
	private JLabel legajoLabel;
	private JLabel labelpass;
	private JPanel panelIngreso;
	
	private EmpleadoPrincipal emplPrincipal;

	private CreacionPrestamos prestamo;
	
	private PagoCuotas pagos;
	
	private Morosos morosos;
	
	public EmpleadoLogin() {
		super();
		initGUI();
	}
	
	/**
	 * Inicializa la interface de login
	 */
	
	private void initGUI() {
		try {
			this.setClosable(true);
	        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	        this.setMaximizable(true);

	        this.setResizable(true);
	        
	        setTitle("Interfaz de Empleado");
	        
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/banco?user=empleado&&password=empleado");
			
			{
				principal = new JPanel();
				getContentPane().add(principal, BorderLayout.CENTER);
				principal.setBackground(new java.awt.Color(172,244,249));
				principal.setLayout(null);
				
				{
					panelIngreso = new JPanel();
					principal.add(panelIngreso);
					panelIngreso.setBounds(0, 0, 284, 112);
					panelIngreso.setLayout(null);
					panelIngreso.setOpaque(false);
					{
						labelpass = new JLabel();
						panelIngreso.add(labelpass);
						labelpass.setText("Password");
						labelpass.setBounds(8, 53, 59, 16);
					}
					{
						legajoLabel = new JLabel();
						panelIngreso.add(legajoLabel);
						legajoLabel.setText("Legajo");
						legajoLabel.setBounds(8, 21, 71, 16);
					}
					{
						passwordField = new JPasswordField();
						panelIngreso.add(passwordField);
						passwordField.setBounds(79, 51, 193, 20);
						passwordField.addActionListener(new OyenteBotonOk());
						//passwordField.setText("knowsnothing");
						/* quitar por defecto*/

					}
					{
						legajoField = new JTextField();
						panelIngreso.add(legajoField);
						legajoField.setBounds(79, 19, 193, 20);
						//legajoField.setText("1000");
						/* quitar por defecto*/
					}
					{
						botonCancel = new JButton();
						panelIngreso.add(botonCancel);
						botonCancel.setText("Cancel");
						botonCancel.setBounds(170, 81, 90, 20);
						botonCancel.addActionListener(new OyenteBotonCancel());
					}
					{
						botonOk = new JButton();
						panelIngreso.add(botonOk);
						botonOk.setText("OK");
						botonOk.setBounds(30, 81, 90, 20);
						botonOk.addActionListener(new OyenteBotonOk());
					}
					{
						emplPrincipal = new EmpleadoPrincipal(this);
						principal.add(emplPrincipal);
						emplPrincipal.setBounds(1,1,1,1);
						
						emplPrincipal.setVisible(false);  
					}
				}			
				
			}
			pack();
			setPreferredSize(new java.awt.Dimension(300,150));
			
			addComponentListener(new ComponentAdapter() {
	            public void componentHidden(ComponentEvent evt) {
	               cerrar();
	            }
	            public void componentShown(ComponentEvent evt) {
	               
	            }
	         });			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error en la inicializacion de EmpleadoLogin",
                    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	
	/**
	 * Se utiliza para el boton Ok de la interfaz de login
	 * Corrobora que los datos ingresados sean correctos y de serlos muestra el resto de la interfaz
	 * @author
	 *
	 */
	
	private class OyenteBotonOk implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			try{
				
				String sLegajo = legajoField.getText();
				if(sLegajo.indexOf("-") == -1){
					legajo = new Integer(sLegajo);
					
					String pass = new String(passwordField.getPassword());
					
					String str = ""
							+"select legajo "
							+"\n from empleado e "
							+"\n where e.legajo = " + legajo
							+"\n and e.password = md5(\"" + pass + "\")"
							;
					
					Statement stm = cnx.createStatement();
					stm.execute(str);
					ResultSet res = stm.getResultSet();
					if(res.next()){
						mostrarPrincipal(panelIngreso);
					}else{
						panelIngreso.setVisible(false);
						legajoField.setVisible(false);
						legajoLabel.setVisible(false);
						legajoLabel = new JLabel();
						panelIngreso.add(legajoLabel);
						legajoLabel.setText("ERROR! ingrese nuevamente su pass");
						legajoLabel.setBounds(30, 19, 264, 16);
						legajoLabel.setForeground(new java.awt.Color(255,0,0));
						legajoLabel.setVisible(true);
						passwordField.setText("");
						panelIngreso.setVisible(true);
					}
					
					stm.close();
					res.close();
				}
			}
			catch(SQLException ex){
				JOptionPane.showMessageDialog(null,
                        ex.getMessage() + "\n", 
                        "Error!",
                        JOptionPane.ERROR_MESSAGE);
			}
			catch(NumberFormatException ex){
				JOptionPane.showMessageDialog(null,
                        "Error en el numero ingresado.\nPor favor ingrese datos correctos", 
                        "Error en nro de legajo",
                        JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
	
	/**
	 * Se utiliza para el boton Cancelar de la interface de login del ATMLogin
	 * @author
	 *
	 */
	
	private class OyenteBotonCancel implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			cerrar();			
		}
		
	}
	
	
	
	/**
	 * Cierra la GUI de forma segura desconectandose de la base de datos
	 */
	
	public void cerrar() {
		desconectarBD();
		this.dispose();		
	}
	
	/**
	 * Hace un close sobre la coneccion a la base de datos
	 */
	
	private void desconectarBD()
	   {
	         try
	         {
	            if(cnx!=null)
	            	cnx.close();  
	         }
	         catch (SQLException ex)
	         {
	        	 JOptionPane.showMessageDialog(null,
	                       ex.getMessage() + "\n", 
	                        "Error en desconectar la BD",
	                        JOptionPane.ERROR_MESSAGE);
	         }      
	   }
	
	public void cargarPrestamo(){
		prestamo= new CreacionPrestamos(cnx, legajo, this);
		principal.setVisible(false);
		emplPrincipal.setVisible(false);
		principal.add(prestamo);
		setSize(prestamo.getPreferredSize().width + 12, prestamo.getPreferredSize().height + 25);
		principal.setVisible(true);
	}
	
	/**
	 * Muestra el panel principal con las opciones de Empleado
	 * @param panel se va a ocultar de EmpleadoLogin
	 */
	
	public void mostrarPrincipal(JPanel panel){
		principal.setVisible(false);
		panel.setVisible(false);
		setSize(emplPrincipal.getPreferredSize().width + 12, emplPrincipal.getPreferredSize().height + 25);
		emplPrincipal.setVisible(true);
		emplPrincipal.setBounds(0, 0, emplPrincipal.getPreferredSize().width, emplPrincipal.getPreferredSize().height);
		principal.setVisible(true);
	}

	public void registrarPago() {
		pagos = new PagoCuotas(cnx, this);
		principal.setVisible(false);
		emplPrincipal.setVisible(false);
		principal.add(pagos);
		setSize(pagos.getPreferredSize().width, pagos.getPreferredSize().height + 25);
		principal.setVisible(true);
	}
	
	public void verMorosos(){
		morosos = new Morosos(cnx, this);
		principal.setVisible(false);
		emplPrincipal.setVisible(false);
		principal.add(morosos);
		setSize(morosos.getPreferredSize().width + 12, morosos.getPreferredSize().height + 25);
		principal.setVisible(true);
	}
	
	
	
}