package principal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Color;

@SuppressWarnings("serial")
public class PagoCuotas extends JPanel {

	private JPanel contentPane;
	
	//Datos que necesito para mostrar pagos del cliente.
	private int nro_doc;
	private int nro_cliente;
	private int nro_prestamo;
	
	private Connection cnx;

	private JTable tabla;

	private JScrollPane ScrollPane;

	private JButton registrar;
	
	private int seleccionado = -1;
	private JPanel ingresoPanel;
	private JPanel panelSeleccion;

	private JLabel seleccionLabel;
	private JTextField paraIngresarDoc;

	private EmpleadoLogin login;
	private JButton btnVolver;


	
	
	
	/**
	 * Create the frame.
	 */
	public PagoCuotas(Connection cnx, EmpleadoLogin login){
		try{
			
			this.cnx = cnx;
			this.login = login;
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			add(contentPane);
			contentPane.setLayout(null);
			setBounds(0,0,700,450);
			setPreferredSize(new java.awt.Dimension(700,450));
	        setLayout(null);
	        contentPane.setBounds(0,0,700,450);
	        
	        this.setOpaque(false);
	        contentPane.setOpaque(false);
			
			
			JPanel principal = new JPanel();
			principal.setBounds(0, 0, 685, 412);
			contentPane.add(principal);
			principal.setLayout(null);
			principal.setOpaque(false);
			
			ingresoPanel = new JPanel();
			ingresoPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
			ingresoPanel.setBounds(10, 10, 665, 34);
			principal.add(ingresoPanel);
			ingresoPanel.setLayout(null);
			
			JLabel clienteLabel = new JLabel();
			clienteLabel.setBounds(10, 0, 110, 35);
			ingresoPanel.add(clienteLabel);
			clienteLabel.setText("Nro de documento:");
			
			JButton buscarButton = new JButton();
			buscarButton.addActionListener(new OyenteBuscar());
			buscarButton.setText("Buscar cliente");
			buscarButton.setBounds(532, 6, 123, 23);
			ingresoPanel.add(buscarButton);
			
			paraIngresarDoc = new JTextField();
			paraIngresarDoc.setBounds(130, 6, 130, 23);
			ingresoPanel.add(paraIngresarDoc);
			paraIngresarDoc.setColumns(10);
			
			panelSeleccion = new JPanel();
			panelSeleccion.setBorder(new LineBorder(new Color(0, 0, 0)));
			panelSeleccion.setBounds(10, 55, 665, 35);
			principal.add(panelSeleccion);
			panelSeleccion.setLayout(null);
			
			
			registrar = new JButton();
			registrar.setBounds(532, 6, 123, 23);
			panelSeleccion.add(registrar);
			registrar.setText("Registrar pago");
			registrar.setEnabled(false);
			
			seleccionLabel = new JLabel("Pago seleccionado:");
			seleccionLabel.setBounds(10, 0, 129, 34);
			panelSeleccion.add(seleccionLabel);
			registrar.addActionListener(new OyenteRegistrar());
			
			tabla = new JTable();
			
			ScrollPane = new JScrollPane(tabla);
			principal.add(ScrollPane);
			ScrollPane.setBounds(3, 95, 685, 317);
			
	       	tabla.setBackground(new java.awt.Color(255,255,128));
	       	
	       	
	        TableModel PagosModel =  // se crea un modelo de tabla PagosModel 
	        new DefaultTableModel  // extendiendo el modelo DefalutTableModel
	        (
	        		new String[][] {},
	        		new String[] {"Nro_de_Pago", "Valor", "Vencimiento"}
	        )
	        {	// con una clase an�nima 
	        	// define la clase java asociada a cada columna de la tabla
	        	Class[] types = new Class[] { java.lang.String.class, java.lang.Float.class, java.lang.String.class };
	        	// define si una columna es editable
	        	boolean[] canEdit = new boolean[] { false, false, false };
	                      
	        	// recupera la clase java de cada columna de la tabla
	        	public Class getColumnClass(int columnIndex) {
	        		return types[columnIndex];
	        	}
	        	// determina si una celda es editable
	        	public boolean isCellEditable(int rowIndex, int columnIndex){
	        		return canEdit[columnIndex];
	        	}
	        }
	        ;
	        
	        ScrollPane.setViewportView(tabla);               
	        tabla.setModel(PagosModel); // setea el modelo de la tabla  
	        tabla.setAutoCreateRowSorter(true); // activa el ordenamiento por columnas, para
	                                            // que se ordene al hacer click en una columna
	        
	        //Se agregan eventos que seleccionan filas
	        tabla.setToolTipText("Click o Espacio para seleccionar el registro.");
	        
	        btnVolver = new JButton("Volver");
	        btnVolver.setBounds(596, 416, 89, 23);
	        contentPane.add(btnVolver);
	        btnVolver.addActionListener(new OyenteVolver());
	        
            tabla.addKeyListener(new KeyAdapter() {
               public void keyTyped(KeyEvent evt) {
                  tablaKeyTyped(evt);
               }
            });
            
            tabla.addMouseListener(new MouseAdapter() {
               public void mouseClicked(MouseEvent evt) {
                  tablaMouseClicked(evt);
               }
            });		
            
            this.setVisible(true);
		}catch(Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error en la inicializacion de PagoCuotas",
                    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void refrescar(){
		try {
			//Se hace la consulta
	        String consulta= ""
					+"select	pa.nro_pago as Nro_de_Pago, pr.valor_cuota as Valor, pa.fecha_venc as Vencimiento"
					+"\n from	prestamo pr natural join pago pa"	
					+"\n where 	pa.fecha_pago is NULL"
					+"\n 		and pr.nro_cliente = ("
					+"\n 			select c.nro_cliente"
					+"\n 			from cliente c"
					+"\n 			where c.nro_doc="+nro_doc+""
					+"\n 		);"
					;

	        Statement stm = this.cnx.createStatement();
	        ResultSet res = stm.executeQuery(consulta);
	         
	        //Se crea la tabla
		    // se recorre el resulset y se actualiza la tabla en pantalla
	        ((DefaultTableModel) this.tabla.getModel()).setRowCount(0);
	        int i = 0;
	        while (res.next()) {
	        	// agrega una fila al modelo de la tabla
	        	((DefaultTableModel) this.tabla.getModel()).setRowCount(i + 1);
	        	//se agregan a la tabla los datos correspondientes cada celda de la fila recuperada
	        	this.tabla.setValueAt(res.getString("Nro_de_Pago"), i, 0);
	        	this.tabla.setValueAt(res.getFloat("Valor"), i, 1);            
	        	this.tabla.setValueAt(res.getString("Vencimiento"), i, 2);
	        	i++;
	        }
	        
	        if (i==0)//Ya pag� todo. 
	        	JOptionPane.showMessageDialog(null,
						"Este cliente ya abon� todas las cuotas, aun asi se tiene registro del prestamo", 
	                    "",
	                    JOptionPane.INFORMATION_MESSAGE);
	        
	        // se cierran los recursos utilizados 
	        res.close();
	        stm.close();
	    }catch (SQLException ex){
	         System.out.println("SQLException: " + ex.getMessage());
	         System.out.println("SQLState: " + ex.getSQLState());
	         System.out.println("VendorError: " + ex.getErrorCode());
	    }
	}
	
	private void tablaMouseClicked(MouseEvent evt){
		//if ((this.tabla.getSelectedRow() != -1) && (evt.getClickCount() == 2))
		if ((this.tabla.getSelectedRow() != -1)){
			this.seleccionarFila();
			registrar.setEnabled(true);
		}
	}
	   
	private void tablaKeyTyped(KeyEvent evt){
		if ((this.tabla.getSelectedRow() != -1) && (evt.getKeyChar() == ' ')){
			this.seleccionarFila();
			registrar.setEnabled(true);
		}
	}
	
	private void seleccionarFila(){
		this.seleccionado = this.tabla.getSelectedRow();
		this.seleccionLabel.setText("Pago seleccionado: "+this.tabla.getValueAt(this.tabla.getSelectedRow(), 0).toString());
	}
	
	private boolean soloNumeros(String s){
		return s.matches("[0-9]+");
	}
	
	private void actualizar(){
		//new Date() es de java y me da la fecha actual. SimpleForm.. la covierte al formato que necesito. 
		java.sql.Date fecha_actual = Fechas.convertirStringADateSQL((new SimpleDateFormat("dd/MM/yyyy")).format(new Date()));
		//int nro_prestamo=10000001;
		int nro_pago = Integer.parseInt(this.tabla.getValueAt(this.seleccionado, 0).toString());
		try{
			//se prepara la sentencia sql
	        String sql = "UPDATE pago " +
	                      "SET fecha_pago = ? " +
	                      "WHERE nro_prestamo = ? and nro_pago = ?";
	         
	        // se crea un sentencia preparada
	        PreparedStatement stm = this.cnx.prepareStatement(sql);
	         
	        // se ligan los par�metros efectivos
	        stm.setDate(1, fecha_actual);
	        stm.setInt(2, nro_prestamo);
	        stm.setInt(3, nro_pago);
			
	        stm.executeUpdate();
	        stm.close();
		} catch (SQLException ex){
			JOptionPane.showMessageDialog(this,
	                                       "Se produjo un error al actualizar el registro.\n" + ex.getMessage(),
	                                       "Error",
	                                       JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private class OyenteRegistrar implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			
			actualizar();
			
			JOptionPane.showMessageDialog(null,
					"Se ha registrado el pago de las cuotas seleccionadas", 
                    "",
                    JOptionPane.INFORMATION_MESSAGE);
			refrescar();
			registrar.setEnabled(false);
		}		
	}
	
	private class OyenteBuscar implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {			
			try {	
				boolean error=false;
				if (soloNumeros(paraIngresarDoc.getText())) {
					nro_doc = Integer.parseInt(paraIngresarDoc.getText());
					
					Statement stm = cnx.createStatement();
					stm.execute("select c.nro_cliente from Cliente c where c.nro_doc = " + nro_doc);
					ResultSet res = stm.getResultSet();
					
					if (res.next()){
						nro_cliente=res.getInt("nro_cliente");
						res.close();
						stm.close();
						//Tiene algun prestamo?
						stm = cnx.createStatement();
						String sql = 	"select p.nro_prestamo " +
			                      		"from cliente c natural join prestamo p " +
			                      		"where c.nro_cliente="+nro_cliente+";";
						stm.execute(sql);
						res = stm.getResultSet();
						if (res.next())
							nro_prestamo=res.getInt("nro_prestamo");
						else
							error=true;
						
						res.close();
						stm.close();
					}else
						error=true;
					
					if (!error){						
						refrescar();
					}else
						JOptionPane.showMessageDialog(null, "El cliente no esta registrado o no posee pr�stamos", "",
								JOptionPane.ERROR_MESSAGE);
					registrar.setEnabled(false);
														
				}else
					JOptionPane.showMessageDialog(null, "Ingrese un n�mero de documento", "",
							JOptionPane.ERROR_MESSAGE);
			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,
                        "Se produjo un error al actualizar el registro.\n" + e.getMessage(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			}		
		}		
	}
	
	private void cerrar(){
		login.mostrarPrincipal(this);
	}
	
	private class OyenteVolver implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			cerrar();
			
		}
		
	}
}
