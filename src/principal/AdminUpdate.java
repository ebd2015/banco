package principal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.*;

import javax.swing.JLabel;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import quick.dbtable.DBTable;



/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class AdminUpdate extends JPanel {

	private JPanel principal;
	private JLabel present;
	private JButton update;
	private JButton insert;
	private JButton delete;
	private DBTable tabla;
	private AdminLogin padre;
	private int fila;
	private JComboBox<String> menuTablas;
	
	/**
	 * 
	 * @param cnx Connection de java.sql, una coneccion a la base de datos
	 * @param tabla DBTable que muestra los resultados de las consutas
	 * @param padre AdminLogin, JInternalFrame que contiene a este panel
	 * @param menuTablas de tipo JComboBox<String> el menu que contiene las tablas de la base de datos
	 */

	public AdminUpdate(DBTable tabla, AdminLogin padre, JComboBox<String> menuTablas) {
		super();
		this.tabla = tabla;
		this.padre = padre;
		this.menuTablas = menuTablas;
		initGUI();
	}
	
	/**
	 * Inicializa la interface del panel de modificacion
	 */
	
	private void initGUI() {
		try {
			
	        
	        {
	        	principal = new JPanel();
	        	this.add(principal);
	        	principal.setLayout(null);
	        	principal.setBackground(new java.awt.Color(0,100,100));
	        	principal.setBounds(204, 0, 581, 540);
	        	{
	        		
	        		principal.add(tabla);
	        		tabla.setBounds(12, 58, 552, 397);
	        	}
	        	{
	        		present = new JLabel();
	        		principal.add(present);
	        		present.setText("Panel de Modificacion de Base de Datos Banco");
	        		present.setFont(new java.awt.Font("Calibri",1,18));
	        		present.setForeground(new java.awt.Color(255,255,255));
	        		present.setBounds(136, 13, 564, 33);
	        	}
	        	{
	        		delete = new JButton();
	    			principal.add(delete);
	    			delete.setText("Borrar");
	    			delete.setBounds(448, 467, 116, 23);
	    			delete.addActionListener(new OyenteDeleteFila());
	    			delete.setEnabled(false);
	    		}
	        	{
	        		update = new JButton();
	        		principal.add(update);
	        		update.setText("Modificar");
	        		update.setBounds(321, 467, 116, 23);
	        		update.addActionListener(new OyenteUpdateFila());
	        		update.setEnabled(false);
	        	}
	        	{
	        		insert = new JButton();
	        		principal.add(insert);
	        		insert.setText("Insert");
	        		insert.setBounds(194, 467, 116, 23);
	        		insert.addActionListener(new OyenteInsert());
	        		insert.setEnabled(false);
	        	}
	        }
	       
			setPreferredSize(new java.awt.Dimension(785, 540));
			this.setLayout(null);
			this.setOpaque(false);

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error en la inicializacion de la GUI",
                    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Habilita los botones de modificacion que estan deshabilitados por defecto
	 */
	
	
	public void habilitarBotones(){
		delete.setEnabled(true);
		update.setEnabled(true);
		insert.setEnabled(true);
		tabla.setEnabled(true);
	}
	
	/**
	 * Elimina de la base de datos la tupla seleccionada
	 * Muestra un mensaje de error si la tupla no se puede Borrar 
	 * @author
	 *
	 */
	
	private class OyenteDeleteFila implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			
			String nombreTabla = menuTablas.getSelectedItem().toString();
			
			int opc = JOptionPane.showOptionDialog(null,"Desea ELIMINAR el elemento seleccionado?",
					  "Eliminando de "+nombreTabla,JOptionPane.YES_NO_CANCEL_OPTION,
					   JOptionPane.WARNING_MESSAGE,null,
					  null,null);
			
			if(opc == 0){
				try{
			
					fila = tabla.getSelectedRow();
					int cantColumnas = tabla.getColumnCount();
					
					String str = "delete from "+nombreTabla+" where ";
					String nombreCol = "";
					String valorAttrib = "";
					int type;
					
					for(int i = 0; i < cantColumnas; i++){
						nombreCol = tabla.getColumn(i).getColumnName();
						
						if(tabla.getValueAt(fila, i) != null){
							valorAttrib = tabla.getValueAt(fila, i).toString();
						
							
							type = tabla.getColumn(i).getType();
							
							if(type == 4 || type == 3){ // es un entero
								str +=  nombreCol+" = "+valorAttrib+" and ";
							}
							else{
								str +=  nombreCol+" = \""+valorAttrib+"\" and ";
							}
						}
						
					}
					
					str = str.substring(0, str.length() - 4);
					
					
					padre.ejecutarMod(str);
					
					
					
					tabla.refresh();
				}
				catch(SQLException ex){
					JOptionPane.showMessageDialog(null, "No se puede eliminar la tupla\n"+ex.getMessage(),"Error.",JOptionPane.ERROR_MESSAGE);

				}
			}
			
		}
		
	}
	
	/**
	 * Muestra un panel de ingreso de datos con los datos de la tupla a modificar y
	 * modifica de la base de datos la tupla seleccionada con los valores de los atributos ingresados 
	 * Muestra un mensaje de error si la tupla no se puede Modificar 
	 * @author
	 *
	 */
	
	private class OyenteUpdateFila implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			String nombreTabla = menuTablas.getSelectedItem().toString();
			fila = tabla.getSelectedRow();
			int cantColumnas = tabla.getColumnCount();
			
			Object[] obj = new Object[cantColumnas * 2 + 2];
			
			String nombreCol = "";
			
			String valorAttrib = "";
			
			for(int i = 0; i < cantColumnas; i++){
				int label = (i + 1) * 2 - 2;
				int text = (i + 1) * 2 - 1;
				nombreCol = tabla.getColumn(i).getColumnName();
				if(tabla.getValueAt(fila, i) != null)
					valorAttrib = tabla.getValueAt(fila, i).toString();
				else
					valorAttrib = "NULL";
				obj[label] = new JLabel(nombreCol);
				JTextField campo = new JTextField(valorAttrib);
				obj[text] = campo;
			}
			
			obj[cantColumnas * 2] = "Aceptar";
			obj[cantColumnas * 2 + 1 ] = "Cancelar";

			
			
			int opc = JOptionPane.showOptionDialog(null,"Modifique los campos y presione Aceptar\nSi desea insertar un campo cifrado recuerde usar la funcion MD5(\"password\") de MYSQL\nde lo contrario deber� eliminar la tupla recientemente insertada",
					  "Modificando "+nombreTabla,JOptionPane.YES_NO_CANCEL_OPTION,
					   JOptionPane.QUESTION_MESSAGE,null,
					  obj,obj[0]);
			

			if(opc == cantColumnas *2){
				try{
					int type;
					String str = "update "+nombreTabla+"\n set ";
					
					//completa clausula set
					
					for(int i = 0; i < cantColumnas; i++){
						nombreCol = tabla.getColumn(i).getColumnName();
						valorAttrib = ((JTextField) obj[i*2 +1]).getText();
						
						type = tabla.getColumn(i).getType();
						
						if(!valorAttrib.equalsIgnoreCase("NULL")){
							String aux = valorAttrib.toLowerCase();
							
							if(type == 4 || type == 3 || aux.indexOf("md5(") != -1){ // es un entero, un float o un encriptado
								str +=  nombreCol+" = "+valorAttrib+" , ";
							}
							else{
								str +=  nombreCol+" = \""+valorAttrib+"\" , ";
							}
						}
						
						
					}
					
					str = str.substring(0, str.length() - 2);
					
					str += "\n where ";
					
					//Completa clausura Where
					
					for(int i = 0; i < cantColumnas; i++){
						nombreCol = tabla.getColumn(i).getColumnName();
						
						if(tabla.getValueAt(fila, i) != null){
							valorAttrib =  tabla.getValueAt(fila, i).toString();;
							
							type = tabla.getColumn(i).getType();
							
							if(type == 4 || type == 3){ // es un entero
								str +=  nombreCol+" = "+valorAttrib+" and ";
							}
							else{
								str +=  nombreCol+" = \""+valorAttrib+"\" and ";
							}
						}
						else{
							str +=  nombreCol+" is null and ";
						}
						
						
					}
					
					str = str.substring(0, str.length() - 4);
					
					padre.ejecutarMod(str);
					
					tabla.refresh();
				}
				catch(SQLException ex){
					JOptionPane.showMessageDialog(null, "Error en el ingreso de Atributos\n" + ex.getMessage(),"Error.",JOptionPane.ERROR_MESSAGE);
				}
			}
			
		}
		
		
		
	}
	
	/**
	 * Muestra un panel de ingreso de datos dependiendo de la tabla seleccionada e
	 * Inserta en la tabla una tupla con los valores ingresados
	 * Muestra un mensaje de error si alguno de los valores no se puede ingresar
	 * @author
	 *
	 */
	
	private class OyenteInsert implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			String nombreTabla = menuTablas.getSelectedItem().toString();
			//fila = tabla.getSelectedRow();
			int cantColumnas = tabla.getColumnCount();
			
			Object[] obj = new Object[cantColumnas * 2 + 2];
			
			String nombreCol = "";
			
			String valorAttrib = "";
			
			for(int i = 0; i < cantColumnas; i++){
				int label = (i + 1) * 2 - 2;
				int text = (i + 1) * 2 - 1;
				nombreCol = tabla.getColumn(i).getColumnName();
				
				obj[label] = new JLabel(nombreCol);
				JTextField campo = new JTextField("<Inserte Valor>");
				campo.addMouseListener(new BorrarTexto(campo));
				obj[text] = campo;
			}
			
			obj[cantColumnas * 2] = "Aceptar";
			obj[cantColumnas * 2 + 1 ] = "Cancelar";

			
			
			int opc = JOptionPane.showOptionDialog(null,"Modifique los campos y presione Aceptar \nSi desea insertar un campo cifrado recuerde usar la funcion MD5(\"password\") de MYSQL\nde lo contrario deber� eliminar la tupla recientemente insertada",
					  "Insertando en "+nombreTabla,JOptionPane.YES_NO_CANCEL_OPTION,
					   JOptionPane.QUESTION_MESSAGE,null,
					  obj,obj[0]);
			

			if(opc == cantColumnas *2){
				try{
					int type;
					String str = "insert into "+nombreTabla+"\n values( ";
					
					//completa clausula set
					
					for(int i = 0; i < cantColumnas; i++){
						nombreCol = tabla.getColumn(i).getColumnName();
						valorAttrib = ((JTextField) obj[i*2 +1]).getText();
						
						type = tabla.getColumn(i).getType();
						
						if(!valorAttrib.equalsIgnoreCase("NULL"))
							
							if(type == 4 || type == 3){ // es un entero
								str +=  valorAttrib+" , ";
							}
							else{
								str +=  "\""+valorAttrib+"\" , ";
							}
						else{
							str += "NULL , ";
						}
						
					}
					
					str = str.substring(0, str.length() - 2);
					
					str += ") ";
					

					padre.ejecutarMod(str);
					
					tabla.refresh();
				}
				catch(SQLException ex){
					JOptionPane.showMessageDialog(null,"Error en el ingreso de Atributos\n" + ex.getMessage(),"Error.",JOptionPane.ERROR_MESSAGE);
				}
			}
			
		}
		
		
		
	}
	
	/**
	 * Se utiliza para borrar el texto del panel de insercion
	 * @author 
	 *
	 */
	
	private class BorrarTexto implements MouseListener{

		private JTextField texto;
		
		public BorrarTexto(JTextField texto){
			this.texto = texto;
		}
		public void mouseClicked(MouseEvent arg0) {
			texto.setText("");
		}
		public void mouseEntered(MouseEvent arg0) {}

		public void mouseExited(MouseEvent arg0) {}

		public void mousePressed(MouseEvent arg0) {}

		public void mouseReleased(MouseEvent arg0) {}
		
	}
	
}