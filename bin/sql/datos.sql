insert into Ciudad values (8000, "Bahia Blanca");
insert into Ciudad values (8200, "General Acha");
insert into Ciudad values (7540, "Coronel Suarez");
insert into Ciudad values (8206, "General San Martin");

insert into Sucursal values (100, "Centro", "Estomba 50", "291-4545454", "10:00 - 15:00", 8000);
insert into Sucursal values (200, "Oeste", "Alem 2000", "291-4000000", "10:00 - 15:00", 8000);
insert into Sucursal values (300, "Sur", "Colon 2500", "291-4343434", "10:00 - 15:00", 8000);
insert into Sucursal values (400, "Pampa Centro", "Campos 600", "2952-432345", "10:00 - 15:00", 8200);
insert into Sucursal values (500, "Ventania", "Mitre 200", "2926-403030", "10:00 - 15:00", 7540);
insert into Sucursal values (600, "Salina", "Rivadavia 100", "2951-431424", "10:00 - 15:00", 8206);

insert into Empleado values(1000, "Nieve", "Juan", "DNI", 30000001, "Estomba 52", "291-4554545", "Gerente", md5("knowsnothing"), 100);
insert into Empleado values(1001, "Stark", "Robb", "DNI", 30000002, "Estomba 52", "291-4554545", "Cajero", md5("knowsnothing"), 100);
insert into Empleado values(1002, "Stark", "Arya", "DNI", 30000003, "Estomba 52", "291-4554545", "Secretario", md5("aguja"), 100);
insert into Empleado values(1003, "Lannister", "Tywin", "DNI", 30000004, "Campos 50", "2952-000000", "Gerente", md5("casterlyrock"), 400);
insert into Empleado values(1004, "Lannister", "Tyrion", "DNI", 30000005, "Campos 500", "2952-525252", "Secretario", md5("shae"), 400);
insert into Empleado values(1005, "Bolton", "Ramsey", "DNI", 30000006, "Sarmiento 39", "2926-412345", "Gerente", md5("reek"), 500);
insert into Empleado values(1006, "Stark", "Sansa", "DNI", 30000007, "Sarmiento 39", "2926-412345", "Limpieza", md5("joffrey"), 500);
insert into Empleado values(1007, "Targaryen", "Daenerys", "DNI", 30000008, "Estomba 52", "2951-412345", "Gerente", md5("dracarys"), 600);
insert into Empleado values(1008, "Mormont", "Jorah", "DNI", 30000009, "Estomba 52", "2951-412345", "Seguridad", md5("dany"), 600);

insert into Cliente values(1, "Stinson", "Barney", "DNI", 31000001, "Estomba 102", "291-4141411", "1970-01-12");
insert into Cliente values(2, "Mosby", "Ted", "DNI", 31000002, "Chiclana 102", "291-4141412", "1970-01-13");
insert into Cliente values(3, "Eriksen", "Marshall", "DNI", 31000003, "Chiclana 102", "291-4141412", "1967-01-12");
insert into Cliente values(4, "Aldrin", "Lily", "DNI", 31000004, "Chiclana 102", "291-4141412", "1975-01-12");
insert into Cliente values(5, "Scherbatsky", "Robin", "DNI", 31000005, "Moreno 102", "291-4141422", "1975-01-12");
insert into Cliente values(6, "Bing", "Chandler", "DNI", 31000006, "Moreno 2", "291-4121412", "1975-05-12");
insert into Cliente values(7, "Tribbiani", "Joey", "DNI", 31000007, "Moreno 2", "291-4121412", "1975-05-15");
insert into Cliente values(8, "Geller", "Ross", "DNI", 31000008, "Moreno 2", "291-4121412", "1975-07-12");
insert into Cliente values(9, "Geller", "Monica", "DNI", 31000009, "Moreno 2", "291-4121412", "1975-07-12");
insert into Cliente values(10, "Green", "Rachel", "DNI", 31000010, "Moreno 2", "291-4121412", "1975-03-12");
insert into Cliente values(11, "Buffay", "Phoebe", "DNI", 31000011, "Moreno 2", "291-4121412", "1975-03-13");
insert into Cliente values(12, "House", "Gregory", "DNI", 31000012, "Zelarrayan 34", "291-4121212", "1965-01-12");
insert into Cliente values(13, "Chase", "Robert", "DNI", 31000013, "Mitre 200", "291-4121421", "1975-03-16");
insert into Cliente values(14, "Foreman", "Eric", "DNI", 31000014, "Mitre 201", "291-4509090", "1985-03-12");
insert into Cliente values(15, "Mulder", "Fox", "DNI", 31000015, "Mitre 1013", "2926-412222", "1981-10-13");
insert into Cliente values(16, "Scully", "Dana", "DNI", 31000016, "Mitre 1013", "2926-412222", "1980-10-13");

insert into Plazo_Fijo values (1, 1000.00, "2015-10-12", "2015-11-12", 5.50, 4.52, 100);
insert into Plazo_Fijo values (2, 100000.00, "2015-10-12", "2015-11-12", 5.55, 456.16, 100);
insert into Plazo_Fijo values (3, 200000.00, "2015-10-12", "2015-12-12", 6.39, 2100.82, 200);
insert into Plazo_Fijo values (4, 10000.00, "2014-11-12", "2015-11-12", 7.50, 73.97, 200);
insert into Plazo_Fijo values (5, 100000.00, "2014-10-12", "2015-10-12", 7.55, 7446.58, 300);
insert into Plazo_Fijo values (6, 1000.00, "2015-10-12", "2015-11-12", 5.50, 4.52, 400);
insert into Plazo_Fijo values (7, 1000.00, "2015-10-12", "2015-11-12", 5.50, 4.52, 400);

insert into Tasa_Plazo_Fijo values (30, 0.00, 60000.00, 5.50);
insert into Tasa_Plazo_Fijo values (30, 60000.01, 150000.00, 5.55);
insert into Tasa_Plazo_Fijo values (30, 150000.01, 999999.99, 5.64);
insert into Tasa_Plazo_Fijo values (60, 0.00, 60000.00, 6.25);
insert into Tasa_Plazo_Fijo values (60, 60000.01, 150000.00, 6.30);
insert into Tasa_Plazo_Fijo values (60, 150000.01, 999999.99, 6.39);
insert into Tasa_Plazo_Fijo values (90, 0.00, 60000.00, 6.50);
insert into Tasa_Plazo_Fijo values (90, 60000.01, 150000.00, 6.55);
insert into Tasa_Plazo_Fijo values (90, 150000.01, 999999.99, 6.64);
insert into Tasa_Plazo_Fijo values (120, 0.00, 60000.00, 6.75);
insert into Tasa_Plazo_Fijo values (120, 60000.01, 150000.00, 6.80);
insert into Tasa_Plazo_Fijo values (120, 150000.01, 999999.99, 6.89);
insert into Tasa_Plazo_Fijo values (180, 0.00, 60000.00, 7.00);
insert into Tasa_Plazo_Fijo values (180, 60000.01, 150000.00, 7.05);
insert into Tasa_Plazo_Fijo values (180, 150000.01, 999999.99, 7.14);
insert into Tasa_Plazo_Fijo values (360, 0.00, 60000.00, 7.50);
insert into Tasa_Plazo_Fijo values (360, 60000.01, 150000.00, 7.55);
insert into Tasa_Plazo_Fijo values (360, 150000.01, 999999.99, 7.64);

insert into Plazo_Cliente values (1, 1);
insert into Plazo_Cliente values (1, 2);
insert into Plazo_Cliente values (1, 3);
insert into Plazo_Cliente values (1, 4);
insert into Plazo_Cliente values (1, 5);
insert into Plazo_Cliente values (2, 11);
insert into Plazo_Cliente values (3, 15);
insert into Plazo_Cliente values (4, 15);
insert into Plazo_Cliente values (4, 16);
insert into Plazo_Cliente values (5, 1);
insert into Plazo_Cliente values (6, 1);
insert into Plazo_Cliente values (7, 12);



insert into Tasa_Prestamo values (6, 0.00, 3000.00, 17.00);
insert into Tasa_Prestamo values (12, 0.00, 3000.00, 18.50);
insert into Tasa_Prestamo values (24, 0.00, 3000.00, 20.00);
insert into Tasa_Prestamo values (60, 0.00, 3000.00, 25.00);
insert into Tasa_Prestamo values (120, 0.00, 3000.00, 30.00);
insert into Tasa_Prestamo values (6, 3000.01, 10000.00, 20.00);
insert into Tasa_Prestamo values (12, 3000.01, 10000.00, 21.50);
insert into Tasa_Prestamo values (24, 3000.01, 10000.00, 23.00);
insert into Tasa_Prestamo values (60, 3000.01, 10000.00, 28.00);
insert into Tasa_Prestamo values (120, 3000.01, 10000.00, 33.00);
insert into Tasa_Prestamo values (6, 10000.01, 30000.00, 24.00);
insert into Tasa_Prestamo values (12, 10000.01, 30000.00, 25.50);
insert into Tasa_Prestamo values (24, 10000.01, 30000.00, 27.00);
insert into Tasa_Prestamo values (60, 10000.01, 30000.00, 32.00);
insert into Tasa_Prestamo values (120, 10000.01, 30000.00, 37.00);

insert into Caja_Ahorro values (20000001, 123456789123456789, 500.50);
insert into Caja_Ahorro values (20000002, 123456789123456780, 500.50);
insert into Caja_Ahorro values (20000003, 123456789123456781, 5000.50);
insert into Caja_Ahorro values (20000004, 123456789123456782, 50.50);

insert into Cliente_CA values (00012,20000001);
insert into Cliente_CA values (00013,20000001);
insert into Cliente_CA values (00014,20000001);
insert into Cliente_CA values (00012,20000002);
insert into Cliente_CA values (00007,20000003);
insert into Cliente_CA values (00008,20000003);
insert into Cliente_CA values (00009,20000003);
insert into Cliente_CA values (00010,20000003);
insert into Cliente_CA values (00011,20000003);
insert into Cliente_CA values (00015,20000004);


insert into Tarjeta values (45,  md5('charPIN01'),md5('charCVT01'),'2015-12-11',00012,20000001);
insert into Tarjeta values (NULL,md5('charPIN01'),md5('charCVT01'),'2015-12-11',00013,20000001);
insert into Tarjeta values (NULL,md5('charPIN03'),md5('charCVT01'),'2015-12-11',00014,20000001);
insert into Tarjeta values (NULL,md5('charPIN02'),md5('charCVT01'),'2015-12-11',00007,20000003);
insert into Tarjeta values (NULL,md5('charPIN02'),md5('charCVT01'),'2015-12-11',00009,20000003);
insert into Tarjeta values (NULL,md5('charPIN01'),md5('charCVT01'),'2015-12-11',00010,20000003);
insert into Tarjeta values (NULL,md5('charPIN01'),md5('charCVT01'),'2015-12-11',00012,20000001);
insert into Tarjeta values (NULL,md5('charPIN01'),md5('charCVT01'),'2015-12-11',00011,20000003);
insert into Tarjeta values (NULL,md5('charPIN01'),md5('charCVT01'),'2015-12-11',00015,20000004);

insert into Caja values (12);
insert into Caja values (NULL);
insert into Caja values (NULL);
insert into Caja values (NULL);
insert into Caja values (NULL);
insert into Caja values (NULL);
insert into Caja values (NULL);
insert into Caja values (NULL);
insert into Caja values (NULL);
insert into Caja values (100);

insert into Ventanilla values(12,100);
insert into Ventanilla values(13,100);
insert into Ventanilla values(14,200);
insert into Ventanilla values(15,200);
insert into Ventanilla values(16,300);
insert into Ventanilla values(17,300);
insert into Ventanilla values(18,300);
insert into Ventanilla values(19,500);

insert into ATM values(12,8000,"Alem 2000");
insert into ATM values(13,8000,'"Estomba 50"');
insert into ATM values(14,8206,'9 de Julio 50');
insert into ATM values(15,8206,'Juan Adagas 525');
insert into ATM values(16,7540,'San Martin 3050');
insert into ATM values(17,7540,'"Alem 325"');
insert into ATM values(18,8206,'Colon 45');
insert into ATM values(19,8000,"Caronti 555");
insert into ATM values(100,8206,'Colon 45');

insert into Transaccion values (1, "2015-01-15", "11:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "12:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:45:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:39:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-16", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-16", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-16", "12:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-16", "11:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-18", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-18", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-18", "10:01:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-18", "12:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "13:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:02:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "15:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-18", "12:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-18", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "18:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-02-18", "10:00:00", 1000.00);
insert into Transaccion values (NULL, "2015-01-15", "10:00:00", 1000.00);

insert into Debito values (1, "Sueldo Marzo", 12, 20000001);
insert into Debito values (2, "Sueldo Abril", 12, 20000001);
insert into Debito values (3, "Sueldo Abril", 13, 20000001);
insert into Debito values (4, "Varios", 14, 20000001);

insert into Transaccion_Por_Caja values (5, 12);
insert into Transaccion_Por_Caja values (6, 13);
insert into Transaccion_Por_Caja values (7, 12);
insert into Transaccion_Por_Caja values (8, 15);
insert into Transaccion_Por_Caja values (9, 12);
insert into Transaccion_Por_Caja values (10, 13);
insert into Transaccion_Por_Caja values (11, 12);
insert into Transaccion_Por_Caja values (12, 15);
insert into Transaccion_Por_Caja values (13, 12);
insert into Transaccion_Por_Caja values (14, 13);
insert into Transaccion_Por_Caja values (15, 12);
insert into Transaccion_Por_Caja values (16, 15);
insert into Transaccion_Por_Caja values (17, 12);
insert into Transaccion_Por_Caja values (18, 15);
insert into Transaccion_Por_Caja values (19, 12);
insert into Transaccion_Por_Caja values (20, 13);
insert into Transaccion_Por_Caja values (21, 12);
insert into Transaccion_Por_Caja values (22, 15);

insert into Deposito values (5, 20000004);
insert into Deposito values (6, 20000004);
insert into Deposito values (7, 20000003);
insert into Deposito values (8, 20000004);

insert into Extraccion values (13,15,20000004);
insert into Extraccion values (14,15,20000004);
insert into Extraccion values (15,15,20000004);
insert into Extraccion values (16, 8, 20000003);

insert into Transferencia values (17,10,20000003,20000001);
insert into Transferencia values (18,00015,20000004,20000001);
insert into Transferencia values (19,00012,20000002,20000001);
insert into Transferencia values (20,00012,20000001,20000003);
insert into Transferencia values (21,00015,20000004,20000002);
insert into Transferencia values (22,00012,20000002,20000004);

insert into Prestamo values (null, "2015-01-15", 9, 1000.00, 20.00, 400.00, 58.33, 1000, 00006);
insert into Prestamo values (null, "2015-01-15", 6, 1000.00, 17.00, 85.00, 180.83, 1000, 00007);
insert into Prestamo values (null, "2015-01-15", 6, 5000.00, 20.00, 500.00, 916.66, 1008, 00016);

insert into Pago values (10000001, 1, "2015-02-15", "2015-02-10");
insert into Pago values (10000001, 2, "2015-03-15", "2015-03-10");
insert into Pago values (10000001, 3, "2015-04-15", NULL);
insert into Pago values (10000001, 4, "2015-05-15", NULL);
insert into Pago values (10000001, 5, "2015-06-15", NULL);
insert into Pago values (10000001, 6, "2015-07-15", NULL);
insert into Pago values (10000001, 7, "2015-08-15",  NULL);
insert into Pago values (10000001, 8, "2015-09-15", NULL);
insert into Pago values (10000001, 9, "2015-10-15", NULL);
insert into Pago values (10000002, 1, "2015-02-15", "2015-02-10");
insert into Pago values (10000002, 2, "2015-03-15", "2015-03-10");
insert into Pago values (10000002, 3, "2015-04-15", "2015-04-10");
insert into Pago values (10000002, 4, "2015-05-15", "2015-05-15");
insert into Pago values (10000002, 5, "2015-06-15", NULL);
insert into Pago values (10000002, 6, "2015-07-15", NULL);
insert into Pago values (10000003, 1, "2015-02-15", "2015-02-10");
insert into Pago values (10000003, 2, "2015-03-15", "2015-03-12");
insert into Pago values (10000003, 3, "2015-04-15", "2015-04-10");
insert into Pago values (10000003, 4, "2015-05-15", "2015-05-10");
insert into Pago values (10000003, 5, "2015-06-15", "2015-06-10");
insert into Pago values (10000003, 6, "2015-07-15", "2015-07-10");











