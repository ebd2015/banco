package principal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import principal.SeleccionFechas;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
@SuppressWarnings("serial")
public class ATMPrincipal extends javax.swing.JPanel {
	private int cod_caja = 100;
	private long nro_caja;
	private JPanel principal;
	private JButton transButton;
	private JButton ExtractButton;
	private JLabel saldoLabel;
	private JLabel atmLabel;
	private Connection cnx;
	private JButton movPeriodo;
	private JButton ultMovimientos;
	private JTable tabla;
	private JScrollPane ScrollPane;
	private SeleccionFechas elegirFecha;
	private TransferenciaPanel transfPanel;
	private long tarjeta;
	private long nro_cliente;
	
	/**
	 * 
	 * @param padre ATMLogin, JInternalFrame que contiene este panel
	 * @param cnx Connection de java.sql, una coneccion a la base de datos
	 */
	
	public ATMPrincipal(Connection cnx) {
		super();
		this.cnx = cnx;
		initGUI();
	}
	
	/**
	 * Inicializa la interface de ATM con los botones para consultar y una tabla
	 */
	
	private void initGUI() {
		try {
			setLayout(null);
			inicializarPrincipal();
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se pudo inicializar la interface\n por un error desconocido","Error de inicializacion",JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	/**
	 * Inicializa los atributos graficos
	 * @throws SQLException si no se puede comunicar con la BD
	 */
	
	private void inicializarPrincipal() throws SQLException{
		{
			principal = new JPanel();
			
			add(principal);
			principal.setBackground(new java.awt.Color(255,255,128));
			principal.setLayout(null);
			principal.setBounds(0, 0, 785, 520);
		}
		{
			elegirFecha = new SeleccionFechas(this);
			principal.add(elegirFecha);
			elegirFecha.setBounds(1,1,1,1);
				
			elegirFecha.setVisible(false);  
		}
		{
			transfPanel = new TransferenciaPanel(this, cnx,cod_caja);
			principal.add(transfPanel);
			transfPanel.setBounds(1,1,1,1);
				
			transfPanel.setVisible(false);  
		}
		{
			atmLabel = new JLabel();
			principal.add(atmLabel);
			atmLabel.setBounds(22, 12, 726, 30);
			atmLabel.setForeground(new java.awt.Color(0,64,0));
			atmLabel.setFont(new java.awt.Font("Segoe UI",1,20));
		}
		{
			saldoLabel = new JLabel();
			saldoLabel.setForeground(new java.awt.Color(0,64,0));
			saldoLabel.setFont(new java.awt.Font("Segoe UI",1,20));
			saldoLabel.setBounds(22, 469, 726, 30);
			principal.add(saldoLabel);
		}
		
		{
			ultMovimientos = new JButton();
			principal.add(ultMovimientos);
			ultMovimientos.setText("Ultimos Movimientos");
			ultMovimientos.setBounds(22, 62, 160, 23);
			ultMovimientos.addActionListener(new OyenteUltMov());
		}
		{
			movPeriodo = new JButton();
			principal.add(movPeriodo);
			movPeriodo.setText("Movimientos / Periodo");
			movPeriodo.setBounds(22, 96, 160, 23);
			movPeriodo.addActionListener(new OyenteUltMovFecha());
		}
		{
			transButton = new JButton();
			transButton.setText("Transferencia");
			transButton.setBounds(22, 130, 160, 23);
			principal.add(transButton);
			
			transButton.addActionListener(new OyenteTransferencia());
		}
		{
			ExtractButton = new JButton();
			ExtractButton.setText("Extraccion");
			ExtractButton.setBounds(22, 164, 160, 23);
			principal.add(ExtractButton);
			ExtractButton.addActionListener(new OyenteExtraccion());
		}
		{
			
			tabla = new JTable();
			ScrollPane = new JScrollPane(tabla);
			principal.add(ScrollPane);
			ScrollPane.setBounds(193, 62, 567, 395);
			
        	tabla.setBounds(172, 62, 580, 350);
        	tabla.setBackground(new java.awt.Color(255,255,128));

		}
		
		setPreferredSize(new java.awt.Dimension(785, 520));
	}
	
	/**
	 * consulta a la base de datos los ultimos movimientos del nro de tarjeta atributo
	 * @param menorFecha fecha con formato dd/mm/yyyy representa al inicio del rango de fechas
	 * @param mayorFecha fecha con formato dd/mm/yyyy representa al final del rango de fechas
	 */
	
	public void ultimosMovimientos(String menorFecha, String mayorFecha,int num){
		try{
	
			mayorFecha = Fechas.invertirFecha(mayorFecha);
			menorFecha = Fechas.invertirFecha(menorFecha);
			if(mayorFecha.compareTo(menorFecha) >= 0){
				//System.out.println(mayorFecha+" "+menorFecha);
				
			    String str = ""
					+" select tr.fecha, tr.hora, tr.tipo, tr.monto, tr.cod_caja, tr.destino "
					+"\n from trans_cajas_ahorro tr"
					+"\n where tr.nro_ca = "+nro_caja
					+"\n and tr.fecha >= \""+menorFecha+ "\" and tr.fecha <= \""+mayorFecha+"\""
					+"\n order by fecha desc, hora desc "
					;
			
			   // System.out.println(str);
			    Statement stm = cnx.createStatement();
			    stm.execute(str);
			    ResultSet res = stm.getResultSet();
	
			    String[][] aux;
			    
			    aux = volcarResultSet(res, num);
			    
				  
			    if(aux!=null)
			    	crearTabla(aux);
	          
				
	  	        res.close();
	  	        stm.close();
			}else{
				JOptionPane.showMessageDialog(null,
		                  "La fecha final es menor que la inicial\n", 
		                  "Error!",
		                  JOptionPane.ERROR_MESSAGE);
			}
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null,
                  "Error desconocido\n", 
                  "Error!",
                  JOptionPane.ERROR_MESSAGE);
		}
	}	
	
	/**
	 * Vuelca en un arreglo de dos dimensiones el ResultSet producto de una consulta SQL
	 * pero de forma que queden invertidas las filas dentro del String[] con respecto a 
	 * el orden original del ResultSet
	 * @param res ResultSet de java.sql
	 * @return arreglo de String de dos dimensiones
	 */
	
	private String[][] volcarResultSet(ResultSet res, int num){
		
		String[][] str = null;
    	
		 try{
			 if(res!=null && res.next()){
				res.last();
			    int max = res.getRow();
			    res.first();
				
				if(num == 0){
					str = new String[max][6];
				}
				else{
					str = new String[num][6];
				}
				 
				 
				int i = str.length -1;
				
				do{
					 
					 String tipo = res.getString("tipo");
				     String monto = res.getString("monto");
				     String destino = res.getString("destino");
				     String cod_caja = res.getString("cod_caja");
				     
				     String fecha = res.getString("fecha");
				     
				     fecha = fecha.replaceAll("-", "/");
				    if(!Fechas.validar(fecha)){
				    	fecha = Fechas.invertirFecha(fecha);
				    	fecha = fecha.replaceAll("-", "/");
				    }
				    java.util.Date f = Fechas.convertirStringADate(fecha);
				     fecha = Fechas.convertirDateAString(f);
				      
				     str[i][0] = fecha;   
				     
				     str[i][1] = res.getString("hora");
				     
				     str[i][2] = tipo;
				      
				     if(tipo.equals("Debito") || tipo.equals("Extraccion") || tipo.equals("Transferencia"))
				    	 str[i][3] = "- "+monto;
				     else
				    	 str[i][3] = monto;
				    
				     if(!cod_caja.equals("NULL"))
				    	 str[i][4] = cod_caja;
				        
				     if(!destino.equals("NULL"))
				    	 str[i][5] = destino;
				         
				     i--;	
		         }while (res.next() && i >= 0);
			 }
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error!",
                    JOptionPane.ERROR_MESSAGE);
		}
		
		return str;
	}
	
	/**
	 * Vuelca los datos del String str en una tabla para ser mostrada en la GUI
	 * @param str arreglo de String de 2 dimensiones con los datos del ResultSet
	 */
	
	private void crearTabla(String[][] str){
		try{
			TableModel tableM = createTableModel();
			tabla.setModel(tableM);
         	
			((DefaultTableModel) tabla.getModel()).setRowCount(0);
			
			int i = 0;
			int j = 0;
			while(i < str.length){
				if(str[i][0] != null){
					((DefaultTableModel) tabla.getModel()).setRowCount(j + 1);
					
			        tabla.setValueAt(str[i][0], j, 0);
			        
			        tabla.setValueAt(str[i][1], j, 1);
			        
			        tabla.setValueAt(str[i][2], j, 2);
			        
			       	tabla.setValueAt(str[i][3], j, 3);
			        
			       	tabla.setValueAt(str[i][4], j, 4);
			        
			        tabla.setValueAt(str[i][5], j, 5);
			        j++;
				}
			i++;
			}
        
		}
		catch(Exception e){
			
			JOptionPane.showMessageDialog(null,
                    e.getMessage() + "\n", 
                    "Error!",
                    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Crea un TableModel para un atributo de tipo JTable, la tabla mostrada en la GUI
	 * este codigo fue extraido del ejemplo batallas visto en clase
	 * @return TableModel, formato para el atributo tabla
	 */
	
	private TableModel createTableModel(){
		TableModel tableM =  
	            new DefaultTableModel
		            (
		                 new String[][] {},
		                 new String[] {"fecha", "hora", "tipo", "monto", "cod_caja", "destino"}
		            )
					{
			             Class[] types = new Class[] { java.lang.String.class, java.lang.String.class,java.lang.String.class,java.lang.String.class,java.lang.String.class,java.lang.String.class};
			             boolean[] canEdit = new boolean[] { false, false, false, false, false, false };
			
			             public Class getColumnClass(int columnIndex)
			             {
			                return types[columnIndex];
			             }
			
			             public boolean isCellEditable(int rowIndex, int columnIndex)
			             {
			                return canEdit[columnIndex];
			             }
			        };
		return tableM;
	}

	/**
	 * Se utiliza para el boton Ultimos Movimientos
	 * @author
	 *
	 */
	
	private class OyenteUltMov implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			
			//creando fecha adelantada un dia al actual
			
			Date fechaActual = new Date();
			
			//pasando Date a String con formato yyyy/mm/dd
			String fechaAct = (new SimpleDateFormat("dd/MM/yyyy")).format(fechaActual);
			
			ultimosMovimientos("02/01/0001",fechaAct,15);
		}
		
	}
	
	/**
	 * Se utiliza para el boton Movimientos/Periodo
	 * @author 
	 *
	 */
	
	private class OyenteUltMovFecha implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			
			elegirFecha.setBounds(250,200,250, 150);
			elegirFecha.setVisible(true);
			
		}
		
	}
	
	/**
	 * Se utiliza para el boton Transferencia
	 * @author 
	 *
	 */
	
	private class OyenteTransferencia implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			
			transfPanel.setBounds(250,200,transfPanel.getPreferredSize().width, transfPanel.getPreferredSize().height+10);
			transfPanel.setVisible(true);
			
		}
		
	}
	
	/**
	 * Se utiliza para el boton Extraccion
	 * @author 
	 *
	 */
	
	private class OyenteExtraccion implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			Object[] obj = new Object[4];
			
			JTextField campo = new JTextField(20);
			JLabel label = new JLabel("Monto: ");

			obj[0] = label;
			obj[1] = campo;
			
			obj[2] = "Aceptar";
			obj[3] = "Cancelar";
			
			int opc = JOptionPane.showOptionDialog(null,"Indique el monto que desea extraer ",
					  "Extraccion",JOptionPane.YES_NO_CANCEL_OPTION,
					   JOptionPane.QUESTION_MESSAGE,null,
					  obj,obj[0]);
			
			if(opc == 2){
				
				try {
					
					String aux1 = campo.getText();
					
					if(aux1.lastIndexOf("-") == -1){
							float monto = new Float(aux1);
							
							if(monto != 0){
							Statement stm = cnx.createStatement();
							stm.execute("set @verd = true");
							stm.execute("set @bool_cli_caja = true");
							
							stm.execute("call extraccion("+nro_caja+", "+nro_cliente+", "+cod_caja+", "+monto+", @verd, @bool_cli_caja)");
		
							stm.execute("select @verd, @bool_cli_caja");
							
							ResultSet res = stm.getResultSet();
							
							res.next();
							
							if(res.getInt("@verd") == 1 && res.getInt("@bool_cli_caja") == 1){	
								
								JOptionPane.showMessageDialog(null,
						                "La extraccion fue realizada con exito\n", 
						                "Exito!",
						                JOptionPane.INFORMATION_MESSAGE);
								actualizar();
							}
							else{
								JOptionPane.showMessageDialog(null,
						                "Su saldo no es suficiente para realizar esta transaccion\n", 
						                "Error en los datos ingresados",
						                JOptionPane.ERROR_MESSAGE);
							}
							
						}
						else{
							JOptionPane.showMessageDialog(null,
					                "El numero ingresado no se puede extraer", 
					                "Error en los datos ingresados",
					                JOptionPane.ERROR_MESSAGE);
						}
					}
					else{
						JOptionPane.showMessageDialog(null,
				                "Por favor ingrese datos validos\n", 
				                "Error en los datos ingresados",
				                JOptionPane.ERROR_MESSAGE);
					}
					
					
				} 
				catch (SQLException e) {
					
				}
				catch(NumberFormatException ex){
					JOptionPane.showMessageDialog(null,
			                "Por favor ingrese datos validos\n", 
			                "Error en los datos ingresados",
			                JOptionPane.ERROR_MESSAGE);
				}
				
			}
			
		}
		
	}
	
	
	/**
	 * Setea el numero de tarjeta a la clase
	 * @param tarjeta numero de tarjeta ingresado
	 */

	public void setTarjeta(long tarjeta) {
		
		try{
			
			Statement stm = cnx.createStatement();
			stm.execute("select t.nro_ca, tr.saldo, tr.nro_cliente from Tarjeta t natural join trans_cajas_ahorro tr where t.nro_tarjeta = "+tarjeta);

			//stm.execute("select t.nro_ca from Tarjeta t where t.nro_tarjeta = "+tarjeta);
			ResultSet res = stm.getResultSet();
			if(res.next()){
				this.tarjeta = tarjeta;
				
				nro_caja = res.getInt("nro_ca");
				nro_cliente = res.getInt("nro_cliente");
				transfPanel.setNro_caja(nro_caja, nro_cliente);
				atmLabel.setText("Nro Tarjeta: "+ATMLogin.CompletarCeros(tarjeta,16)+"      -------      nro Caja de Ahorro: " +nro_caja);
				
				
				
					saldoLabel.setText("Su saldo es de $ "+res.getFloat("saldo"));
				
			}
			res.close();
			stm.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	public void actualizar() {
		try{
			
			Statement stm = cnx.createStatement();
			stm.execute("select tr.saldo from trans_cajas_ahorro tr where tr.nro_ca = "+nro_caja);
			ResultSet res = stm.getResultSet();
			if(res.next()){
				saldoLabel.setText("Su saldo es de $ "+res.getFloat("saldo"));
			}
			res.close();
			stm.close();
			ultMovimientos.doClick();
		}
		catch(SQLException e){
			e.printStackTrace();
		}

		
	}
	
	/*
	
	private JButton getJButton1() {
		if(ExtractButton == null) {
			ExtractButton = new JButton();
			ExtractButton.setText("ExtractButton");
			ExtractButton.setBounds(22, 164, 160, 23);
		}
		return ExtractButton;
	}
*/
}
